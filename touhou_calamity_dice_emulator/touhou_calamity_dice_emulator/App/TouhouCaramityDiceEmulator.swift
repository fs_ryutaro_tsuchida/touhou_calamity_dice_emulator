//
//  TouhouCaramityDiceEmulator.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 東方Caramityダイスエミュレータ(アプリ)
@main
struct TouhouCaramityDiceEmulator: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                TopView()
            }
        }
    }
}
