//
//  SummonCostAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚コスト表示
struct SummonCostAbility: ScrollItem {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤーID
    let playerId: UUID
    /// カード
    let card: GameModel.GameCard
    /// 召喚コストを変動させる能力一覧
    let abilities: [Int: ManipulateSummonCostAbilityProtocol]

    /// 選択中の複合資材一覧
    @Binding var selectMultiReserves: [Int: MultiReserve]
    /// 選択中の資材一覧
    @Binding var selectReservesInAbilities: [Int: [MultiReserve: [Int: SingleReserve]]]
    /// 選択可能な資材一覧
    let filterReservesInAbilities: [Int: [MultiReserve: [Int: [SingleReserve]]]]

    /// 実際に取り除くコスト
    let removeCost: [SingleReserve: Int]

    let axis: Axis.Set = .vertical
    let showsIndicators = false

    func scrollBody(_ proxy: GeometryProxy) -> some View {
        LazyVStack(spacing: 0, pinnedViews: [.sectionHeaders, .sectionFooters]) {
            Section {
                ForEach(abilities.sorted {
                    $0.key < $1.key
                }, id: \.key) { index, ability in
                    CostAbilityCell(
                        game: $game,
                        card: card,
                        ability: ability,
                        selectMultiReserve: .init(get: {
                            selectMultiReserves[index, default: .anyReserve]
                        }, set: { multiReserve in
                            selectMultiReserves[index] = multiReserve
                        }),
                        selectReservesInMultiReserves: .init(get: {
                            selectReservesInAbilities[index, default: [:]]
                        }, set: { reserves in
                            selectReservesInAbilities[index] = reserves
                        }),
                        filterReservesInMultiReserves: filterReservesInAbilities[
                            index,
                            default: [:]
                        ]
                    )
                }
            } header: {
                UserReserveCollection(
                    player: $game.game.players[id: playerId],
                    reserves: SingleReserve.userReserves,
                    width: proxy.size.width
                )
                .background {
                    Color(.systemBackground)
                }
            } footer: {
                UserReserveCollection(
                    player: $game.game.players[id: playerId],
                    reserves: SingleReserve.userReserves,
                    additionReserves: removeCost.reduce(into: [SingleReserve: Int]()) {
                        $0[$1.key, default: 0] -= $1.value
                    },
                    width: proxy.size.width
                )
                .background {
                    Color(.systemBackground)
                }
            }
        }
    }
}
