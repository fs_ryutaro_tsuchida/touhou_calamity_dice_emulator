//
//  SCA+CostAbilityCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension SummonCostAbility {
    /// 召喚コスト表示のセル
    struct CostAbilityCell: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// カード
        let card: GameModel.GameCard
        /// 召喚コストを変動させる能力
        let ability: ManipulateSummonCostAbilityProtocol

        /// 複合資材一覧
        var multiReserves: [MultiReserve] {
            Array(ability.cost($game, card: card).keys)
        }

        /// 選択中の複合資材一覧
        @Binding var selectMultiReserve: MultiReserve
        /// 選択中の資材一覧
        @Binding var selectReservesInMultiReserves: [MultiReserve: [Int: SingleReserve]]
        /// 選択可能な資材一覧
        let filterReservesInMultiReserves: [MultiReserve: [Int: [SingleReserve]]]

        var body: some View {
            VStack(alignment: .leading) {
                Text(ability.text)
                    .foregroundColor(Color(.secondaryLabel))
                if filterReservesInMultiReserves.values.contains(where: { reserves in
                    reserves.values.contains { !$0.isEmpty }
                }) {
                    MultiReserveSelect(
                        multiReserves: multiReserves,
                        selectMultiReserve: $selectMultiReserve,
                        selectReservesInMultiReserves: $selectReservesInMultiReserves,
                        filterReservesInMultiReserves: filterReservesInMultiReserves
                    )
                }
            }
        }
    }
}
