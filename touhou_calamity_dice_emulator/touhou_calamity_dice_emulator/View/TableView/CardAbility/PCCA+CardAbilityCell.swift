//
//  PCCA+CardAbilityCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.CardAbilityList {
    /// カード能力セル
    struct CardAbilityCell: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// カードID
        let cardId: UUID

        /// カード
        var card: GameModel.GameCard {
            game.game.cards[id: cardId]
        }

        /// 追加された能力
        let additionalAbility: AdditionalAbility
        /// 能力
        var ability: AbilityProtocol {
            additionalAbility.ability(game)
        }

        /// アニメーション用のネームスペース
        let nameSpace: Namespace.ID

        var body: some View {
            Divider()
            if let ability = ability as? RunnableAbilityProtocol {
                Button {
                    guard let playerId = card.fieldPlayerId(game.game) else {
                        return
                    }
                    withAnimation {
                        ability.run(
                            $game,
                            fromPlayerId: playerId,
                            fromCardId: cardId,
                            traceCardId: additionalAbility.traceCardId
                        ) {
                            game.game.cards[id: cardId]
                                .additionalAbilities.remove(id: additionalAbility.id)
                            if !card.isUserOperatorOnShowAbilities,
                               card.additionalAbilities.isEmpty {
                                game.game.cards[id: cardId]
                                    .isShowAbilities = false
                            }
                            $game.nextStepIfCan()
                        }
                    }
                } label: {
                    title
                }
            } else {
                title
            }
        }

        /// タイトル表示
        var title: some View {
            HStack {
                Text(ability.text)
                    .font(.body)
                    .foregroundColor(.secondary)
                    .padding(.vertical, 10)

                Spacer()

                NumberBadgeIcon(value: 1)
                    .matchedGeometryEffect(
                        id: ability.id,
                        in: nameSpace
                    )
            }
            .padding(.horizontal, 15)
        }
    }
}
