//
//  PC+CardAbilityList.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard {
    /// カード能力一覧表示
    struct CardAbilityList: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// カードID
        let cardId: UUID

        /// カード
        var card: GameModel.GameCard {
            game.game.cards[id: cardId]
        }

        /// アニメーション用のネームスペース
        let nameSpace: Namespace.ID

        var body: some View {
            if !card.isShowAbilities {
                EmptyView()
            } else {
                ForEach(card.additionalAbilities) { ability in
                    CardAbilityCell(
                        game: $game,
                        cardId: cardId,
                        additionalAbility: ability,
                        nameSpace: nameSpace
                    )
                }
                ForEach(card.card.abilities, id: \.id) { ability in
                    Divider()
                    HStack {
                        Text(ability.text)
                            .font(.body)
                            .foregroundColor(.secondary)
                            .padding(.vertical, 10)

                        Spacer()
                    }
                    .padding(.horizontal, 15)
                }
            }
        }
    }
}
