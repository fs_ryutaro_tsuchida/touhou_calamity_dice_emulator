//
//  SpellGageGetReserve.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 資材獲得表示
struct SpellGageGetReserve: View {
    /// 獲得資材一覧
    @Binding var selectReserves: [SingleReserve]

    var body: some View {
        LazyVStack {
            ForEach(selectReserves.indices, id: \.self) { index in
                SingleReserveSelect(
                    reserves: SingleReserve.materials,
                    selectReserve: $selectReserves[index],
                    selectFrameColor: .label
                )
            }
        }
    }
}
