//
//  SCCS+CardCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension SummonCard.CardSection {
    /// 召喚カードセル
    struct CardCell: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID

        /// プレイヤー
        var player: Player {
            game.game.players[id: playerId]
        }

        /// カードID
        let cardId: UUID

        /// カード
        var card: GameModel.GameCard {
            game.game.cards[id: cardId]
        }

        /// エラー表示中か
        @State private var isPresentedError = false
        /// エラータイトル
        @State private var errorTitle: String = "" {
            didSet {
                if !errorTitle.isEmpty {
                    isPresentedError = true
                }
            }
        }

        /// エラーメッセージ
        @State private var errorMessage: String = ""

        /// 横幅
        let width: CGFloat

        /// コスト画面を表示しているか
        @State private var isActiveCostView = false

        var body: some View {
            VStack {
                HStack {
                    Button {
                        withAnimation {
                            game.game.cards[id: cardId].isShowAbilities.toggle()
                        }
                    } label: {
                        Image(systemName: "chevron.right.square.fill")
                            .renderingMode(.template)
                            .foregroundColor(.blue)
                            .scaleEffect(x: 1.5, y: 1.5, anchor: .center)
                            .rotationEffect(.degrees(card.isShowAbilities ? 90 : 0))
                    }
                    .padding(.horizontal, 15)

                    NavigationLink(
                        isActive: $isActiveCostView
                    ) {
                        SummonCostView(
                            game: $game,
                            playerId: playerId,
                            cardId: cardId,
                            isActive: $isActiveCostView
                        )
                    } label: {
                        HStack {
                            Text(card.card.name)
                                .font(.body)
                                .foregroundColor(.primary)
                                .padding(.vertical, 10)
                                .padding(.horizontal, 15)

                            Spacer()
                        }
                    }
                }

                Divider()
                CardCostCollection(costs: card.card.costs)

                if card.isShowAbilities {
                    ForEach(card.card.abilities, id: \.id) { ability in
                        Divider()
                        Text(ability.text)
                            .font(.body)
                            .foregroundColor(.secondary)
                            .padding(.vertical, 10)
                            .padding(.horizontal, 15)
                    }
                }
            }
            .alert(
                errorTitle,
                isPresented: $isPresentedError,
                actions: {
                    Button("OK") {}
                }, message: {
                    Text(errorMessage)
                }
            )
            Divider()
        }
    }
}
