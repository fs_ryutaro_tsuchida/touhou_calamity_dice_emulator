//
//  SC+CardSection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension SummonCard {
    /// 召喚カード一覧セクション
    struct CardSection: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID

        /// カードのID一覧
        var cardIds: [UUID] {
            game.game.players[id: playerId].handCardIds +
                game.game.unknownCardIds
        }

        /// 重複を除いたカードのID一覧
        var uniqueCardId: [UUID] {
            cardIds.filter { cardId in
                cardIds.first {
                    game.game.cards[id: cardId].cardId == game.game.cards[id: $0].cardId
                } == cardId
            }.sorted {
                game.game.cards[id: $0].cardId < game.game.cards[id: $1].cardId
            }
        }

        /// 横幅
        var width: CGFloat

        var body: some View {
            if uniqueCardId.isEmpty {
                EmptyView()
            } else {
                Section {
                    ForEach(uniqueCardId, id: \.self) { cardId in
                        CardCell(
                            game: $game,
                            playerId: playerId,
                            cardId: cardId,
                            width: width
                        )
                    }
                } header: {
                    CardHeader(
                        game: game,
                        playerId: playerId,
                        width: width
                    )
                }
            }
        }
    }
}
