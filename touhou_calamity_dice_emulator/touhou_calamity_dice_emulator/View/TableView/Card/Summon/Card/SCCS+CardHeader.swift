//
//  SCCS+CardHeader.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension SummonCard.CardSection {
    /// 召喚カード一覧ヘッダ
    struct CardHeader: View {
        /// ゲーム表示画面の表示データ
        let game: GameViewModel
        /// プレイヤーID
        let playerId: UUID

        /// カードのID一覧
        var cardIds: [UUID] {
            game.game.players[id: playerId].handCardIds +
                game.game.unknownCardIds
        }

        /// 他プレイヤーの手札数
        var otherPlayerHandCards: Int {
            game.game.players.filter {
                $0.id != playerId
            }.reduce(into: 0) {
                $0 += $1.reserves[.handCard, default: 0]
            }
        }

        /// 枚数
        var count: Int {
            cardIds.count - otherPlayerHandCards
        }

        /// 横幅
        let width: CGFloat

        var body: some View {
            Text("カード \(count)枚")
                .foregroundColor(
                    Color(
                        UIColor(named: "ColerCardTableSectionTitleCardList")
                            ?? .label
                    )
                )
                .padding(.vertical, 5)
                .frame(width: width)
                .background(
                    Color(
                        UIColor(named: "ColerCardTableSectionBackgroundCardList")
                            ?? .systemBackground
                    )
                )
        }
    }
}
