//
//  SummonCard.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚カード一覧表示
struct SummonCard: ScrollItem {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤーID
    let playerId: UUID

    let axis: Axis.Set = .vertical
    @State var showsIndicators = false

    func scrollBody(_ proxy: GeometryProxy) -> some View {
        LazyVStack(spacing: 0, pinnedViews: .sectionHeaders) {
            CardSection(game: $game, playerId: playerId, width: proxy.size.width)
        }
    }
}
