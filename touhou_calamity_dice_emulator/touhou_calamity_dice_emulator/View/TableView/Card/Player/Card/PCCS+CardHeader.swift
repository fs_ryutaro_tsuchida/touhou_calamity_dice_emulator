//
//  PCCS+CardHeader.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.CardSection {
    /// プレイヤーカード表示ヘッダ
    struct CardHeader: View {
        /// ゲーム表示画面の表示データ
        let game: GameViewModel
        /// プレイヤーID
        let playerId: UUID

        /// 場に出ているカードの数
        var fieldCount: Int {
            game.game.players[id: playerId].fieldCardIds.count
        }

        /// ゲージ数
        var gageCount: Int {
            game.game.players[id: playerId].reserves[.gage, default: 0]
        }

        /// 横幅
        var width: CGFloat

        var body: some View {
            Text("カード \(fieldCount)/\(gageCount)")
                .foregroundColor(
                    Color(
                        UIColor(named: "ColerCardTableSectionTitleCardList")
                            ?? .label
                    )
                )
                .padding(.vertical, 5)
                .frame(width: width)
                .background(
                    Color(
                        UIColor(named: "ColerCardTableSectionBackgroundCardList")
                            ?? .systemBackground
                    )
                )
        }
    }
}
