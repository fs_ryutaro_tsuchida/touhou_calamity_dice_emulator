//
//  PCCS+CardCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.CardSection {
    /// プレイヤーカードセル
    struct CardCell: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// カードID
        let cardId: UUID

        /// カード
        var card: GameModel.GameCard {
            game.game.cards[id: cardId]
        }

        /// アニメーション用のネームスペース
        @Namespace private var nameSpace

        var body: some View {
            VStack(spacing: 0) {
                Button {
                    withAnimation {
                        game.game.cards[id: cardId].isShowAbilities.toggle()
                        game.game.cards[id: cardId].isUserOperatorOnShowAbilities = true
                    }
                } label: {
                    HStack {
                        Image(systemName: "chevron.right")
                            .renderingMode(.template)
                            .foregroundColor(.primary)
                            .rotationEffect(.degrees(card.isShowAbilities ? 90 : 0))

                        Text(card.card.name)
                            .font(.body)
                            .foregroundColor(.primary)
                            .padding(.vertical, 10)

                        Spacer()

                        if !card.isShowAbilities {
                            NumberBadgeIcon(value: card.additionalAbilities.count)
                                .matchedGeometryEffect(
                                    id: card.additionalAbilities.first?.id ?? .init(),
                                    in: nameSpace
                                )
                        }
                    }
                    .padding(.horizontal, 15)
                }

                PlayerCard.CardAbilityList(
                    game: $game,
                    cardId: cardId,
                    nameSpace: nameSpace
                )
            }
            Divider()
        }
    }
}
