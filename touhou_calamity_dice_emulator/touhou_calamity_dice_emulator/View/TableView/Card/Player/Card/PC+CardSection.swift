//
//  PC+CardSection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard {
    /// プレイヤーカード表示セクション
    struct CardSection: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID
        /// カードのID一覧
        var cardIds: [UUID] {
            game.game.players[id: playerId].fieldCards(game.game).sorted {
                $0.cardId < $1.cardId
            }.map(\.id)
        }

        /// 横幅
        var width: CGFloat

        var body: some View {
            if cardIds.isEmpty {
                EmptyView()
            } else {
                Section {
                    ForEach(cardIds, id: \.self) { cardId in
                        CardCell(game: $game, cardId: cardId)
                    }
                } header: {
                    CardHeader(game: game, playerId: playerId, width: width)
                }
            }
        }
    }
}
