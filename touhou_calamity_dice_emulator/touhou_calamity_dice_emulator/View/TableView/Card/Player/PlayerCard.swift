//
//  PlayerCard.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤーのカード一覧表示
struct PlayerCard: ScrollItem {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤーID
    let playerId: UUID

    let axis: Axis.Set = .vertical
    @State var showsIndicators = false

    func scrollBody(_ proxy: GeometryProxy) -> some View {
        LazyVStack(spacing: 0, pinnedViews: .sectionHeaders) {
            PlayerAbilitySection(game: $game, playerId: playerId, width: proxy.size.width)
            ActionSection(game: $game, playerId: playerId, width: proxy.size.width)
            CardSection(game: $game, playerId: playerId, width: proxy.size.width)
        }
    }
}
