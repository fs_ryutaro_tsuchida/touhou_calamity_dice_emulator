//
//  PC+PlayerAbilitySection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard {
    /// プレイヤー能力表示セクション
    struct PlayerAbilitySection: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID
        /// プレイヤー
        var player: Player {
            game.game.players[id: playerId]
        }

        /// 横幅
        var width: CGFloat

        var body: some View {
            if player.additionalAbilities.isEmpty {
                EmptyView()
            } else {
                Section {
                    ForEach(player.additionalAbilities) { ability in
                        PlayerAbilityCell(
                            game: $game,
                            playerId: playerId,
                            additionalAbility: ability
                        )
                    }
                } header: {
                    PlayerAbilityHeader(
                        game: game,
                        playerId: playerId,
                        width: width
                    )
                }
            }
        }
    }
}
