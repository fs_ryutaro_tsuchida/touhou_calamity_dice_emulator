//
//  PCPAS+PlayerAbilityCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.PlayerAbilitySection {
    /// プレイヤー能力表示セル
    struct PlayerAbilityCell: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID
        /// プレイヤー
        var player: Player {
            game.game.players[id: playerId]
        }

        /// 追加された能力
        let additionalAbility: AdditionalAbility
        /// 能力
        var ability: AbilityProtocol {
            additionalAbility.ability(game)
        }

        var body: some View {
            Divider()
            if let ability = ability as? RunnableAbilityProtocol {
                Button {
                    withAnimation {
                        ability.run(
                            $game,
                            fromPlayerId: playerId,
                            fromCardId: nil,
                            traceCardId: additionalAbility.traceCardId
                        ) {
                            game.game.players[id: playerId]
                                .additionalAbilities.remove(id: additionalAbility.id)
                            $game.nextStepIfCan()
                        }
                    }
                } label: {
                    title
                }
            } else {
                title
            }
        }

        /// タイトル表示
        var title: some View {
            HStack {
                Text(ability.text)
                    .font(.body)
                    .foregroundColor(.secondary)
                    .padding(.vertical, 10)

                Spacer()

                NumberBadgeIcon(value: 1)
            }
            .padding(.horizontal, 15)
        }
    }
}
