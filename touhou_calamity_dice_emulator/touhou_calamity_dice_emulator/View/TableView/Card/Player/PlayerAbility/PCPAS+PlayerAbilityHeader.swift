//
//  PCPAS+PlayerAbilityHeader.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.PlayerAbilitySection {
    /// プレイヤー能力表示ヘッダー
    struct PlayerAbilityHeader: View {
        /// ゲーム表示画面の表示データ
        let game: GameViewModel
        /// プレイヤーID
        let playerId: UUID
        /// プレイヤー
        var player: Player {
            game.game.players[id: playerId]
        }

        /// 横幅
        var width: CGFloat

        var body: some View {
            Text("プレイヤー \(player.additionalAbilities.count)")
                .foregroundColor(
                    Color(
                        UIColor(named: "ColerCardTableSectionTitlePlayerAbilityList")
                            ?? .label
                    )
                )
                .padding(.vertical, 5)
                .frame(width: width)
                .background(
                    Color(
                        UIColor(named: "ColerCardTableSectionBackgroundPlayerAbilityList")
                            ?? .systemBackground
                    )
                )
        }
    }
}
