//
//  PCAS+ActionHeader.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection {
    /// プレイヤーアクションヘッダ
    struct ActionHeader: View {
        /// 横幅
        var width: CGFloat

        var body: some View {
            Text("アクション")
                .foregroundColor(
                    Color(
                        UIColor(named: "ColerCardTableSectionTitleAction")
                            ?? .label
                    )
                )
                .padding(.vertical, 5)
                .frame(width: width)
                .background(
                    Color(
                        UIColor(named: "ColerCardTableSectionBackgroundAction")
                            ?? .systemBackground
                    )
                )
        }
    }
}
