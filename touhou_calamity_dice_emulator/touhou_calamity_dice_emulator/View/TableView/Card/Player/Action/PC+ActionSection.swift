//
//  PC+ActionSection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard {
    /// プレイヤーアクション表示セクション
    struct ActionSection: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID

        /// 横幅
        var width: CGFloat

        var body: some View {
            if game.game.nowTurn != playerId {
                EmptyView()
            } else {
                Section {
                    PlayerDiceRoll(game: $game, playerId: playerId, width: width)
                    SummonCell(game: $game, playerId: playerId, width: width)
                    SpellGage(game: $game, playerId: playerId, width: width)
                    T4C(game: $game, playerId: playerId, width: width)
                    NextTurn(game: $game, playerId: playerId, width: width)
                } header: {
                    ActionHeader(width: width)
                }
            }
        }
    }
}
