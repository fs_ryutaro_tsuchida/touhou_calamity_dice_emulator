//
//  PlayerCardAlertActionCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 警告表示ありのプレイヤーアクションセル
protocol PlayerCardAlertActionCell: PlayerCardActionCell {
    /// 警告のキャンセルボタンを表示するか
    var isShowAlertCancel: Bool { get }
    /// 警告のOKボタン押下時処理
    func onTapAlertOK()
}

extension PlayerCardAlertActionCell {
    @ViewBuilder var body: some View {
        if isHidden {
            EmptyView()
        } else if let reason = disabledReason {
            Button {
                alertTitleBinding.wrappedValue = reason
                withAnimation {
                    isPresentedAlertBinding.wrappedValue = true
                }
            } label: {
                Text(title)
                    .foregroundColor(.gray)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .frame(width: width)
            }
            .alert(
                alertTitleBinding.wrappedValue,
                isPresented: isPresentedAlertBinding
            ) {
                Button("OK") {}
            }
            Divider()
        } else {
            Button(action: onTap) {
                Text(title)
                    .foregroundColor(.init(titleColor))
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .frame(width: width)
            }
            .alert(
                alertTitleBinding.wrappedValue,
                isPresented: isPresentedAlertBinding
            ) {
                if isShowAlertCancel {
                    Button("キャンセル") {}
                }
                Button("OK", action: onTapAlertOK)
            }
            Divider()
        }
    }
}
