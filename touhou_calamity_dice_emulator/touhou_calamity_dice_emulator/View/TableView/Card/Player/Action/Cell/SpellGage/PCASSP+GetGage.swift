//
//  PCASSP+GetGage.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection.SpellGage {
    /// SG1 - G2を得るセル
    struct GetGage: PlayerCardAlertActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "SG1:G2を得る"
        let titleColor: UIColor = .magenta
        let isShowAlertCancel = true

        /// 非表示か
        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return player.reserves[.spellGage, default: 0] < 1
        }

        var disabledReason: String? {
            if player.reserves[.gage, default: 0] >=
                player.maxReserves[.gage, default: SingleReserve.gage.defaultMaxValue] {
                return "既にゲージ獲得量が上限に達しています。"
            }

            return disabledReasonDefault
        }

        /// 警告メッセージ
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            if player.reserves[.gage, default: 0] + 2 >
                player.maxReserves[.gage, default: SingleReserve.gage.defaultMaxValue] {
                alertTitle = "一部のゲージが溢れますが、\nよろしいですか？"
            } else {
                alertTitle = "ゲージを獲得しますか？"
            }
            withAnimation {
                isPresentedAlert = true
            }
        }

        func onTapAlertOK() {
            withAnimation {
                game.game.players[id: playerId].reserves[.gage, default: 0] += 2
                game.game.players[id: playerId].reserves[.spellGage, default: 0] -= 1
            }
        }
    }
}
