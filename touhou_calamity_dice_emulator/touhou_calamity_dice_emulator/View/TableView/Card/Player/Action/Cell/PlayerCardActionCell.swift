//
//  PlayerCardActionCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤーアクションセル
protocol PlayerCardActionCell: View {
    /// ゲーム表示画面の表示データ
    var game: GameViewModel { get }
    /// プレイヤーID
    var playerId: UUID { get }
    /// タイトル
    var title: String { get }
    /// タイトル表示色
    var titleColor: UIColor { get }
    /// 非表示か
    var isHidden: Bool { get }
    /// 無効表示の理由
    var disabledReason: String? { get }
    /// 警告タイトル
    var alertTitleBinding: Binding<String> { get }
    /// 警告表示中か
    var isPresentedAlertBinding: Binding<Bool> { get }
    /// 横幅
    var width: CGFloat { get }
    /// タップ時処理
    func onTap()
}

extension PlayerCardActionCell {
    /// プレイヤー
    var player: Player {
        game.game.players[id: playerId]
    }

    @ViewBuilder var body: some View {
        if isHidden {
            EmptyView()
        } else if let reason = disabledReason {
            Button {
                alertTitleBinding.wrappedValue = reason
                withAnimation {
                    isPresentedAlertBinding.wrappedValue = true
                }
            } label: {
                Text(title)
                    .foregroundColor(.gray)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .frame(width: width)
            }
            .alert(
                alertTitleBinding.wrappedValue,
                isPresented: isPresentedAlertBinding
            ) {
                Button("OK") {}
            }
            Divider()
        } else {
            Button(action: onTap) {
                Text(title)
                    .foregroundColor(.init(titleColor))
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .frame(width: width)
            }
            Divider()
        }
    }

    /// 無効表示の理由の初期値
    var disabledReasonDefault: String? {
        if game.game.cards.filter({ card in
            card.fieldPlayerId(game.game) != nil
        }).contains(where: { card in
            card.additionalAbilities.contains {
                $0.ability(game).isCancelNextStep
            }
        }) {
            return "未解決のカード能力があります。"
        }
        return nil
    }
}
