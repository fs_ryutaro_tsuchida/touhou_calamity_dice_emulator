//
//  PCAS+SpellGage.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection {
    /// スペルゲージセル群
    struct SpellGage: View {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// プレイヤーID
        let playerId: UUID
        /// 横幅
        let width: CGFloat

        @ViewBuilder
        var body: some View {
            GetGage(game: $game, playerId: playerId, width: width)
            GetMaterial(game: $game, playerId: playerId, width: width)
            Draw1Top3(game: $game, playerId: playerId, width: width)
            OnActiveCalamity(game: $game, playerId: playerId, width: width)
        }
    }
}
