//
//  PCAS+T4C.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection {
    /// T4Cセル
    struct T4C: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "T4C"
        let titleColor: UIColor = .cyan

        /// 非表示か
        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return false
        }

        var disabledReason: String? {
            disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            // TODO: タップ時処理
        }
    }
}
