//
//  PCAS+SummonCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection {
    /// 召喚セル
    struct SummonCell: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "召喚する"
        let titleColor: UIColor = .purple

        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return player.reserves[.handCard, default: 0] < 1
        }

        var disabledReason: String? {
            if player.additionalAbilities.contains(where: {
                $0.ability(game) is CannotSummonAbility
            }) {
                return "召喚不可です。"
            }
            return disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            withAnimation {
                game.sheet.summon.id = playerId
                game.sheet.viewType = .summon
            }
        }
    }
}
