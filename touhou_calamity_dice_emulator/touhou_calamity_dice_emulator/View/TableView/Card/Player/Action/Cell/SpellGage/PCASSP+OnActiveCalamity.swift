//
//  PCASSP+OnActiveCalamity.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection.SpellGage {
    /// SG2 - 任意の異変を発動する
    struct OnActiveCalamity: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "SG2:任意の異変を発動する"
        let titleColor: UIColor = .red

        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return player.reserves[.spellGage, default: 0] < 2
        }

        var disabledReason: String? {
            disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            // TODO: タップ時処理
        }
    }
}
