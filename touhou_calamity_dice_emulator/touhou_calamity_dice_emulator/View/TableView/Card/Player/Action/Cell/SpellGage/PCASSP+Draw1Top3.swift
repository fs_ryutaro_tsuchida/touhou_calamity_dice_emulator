//
//  PCASSP+Draw1Top3.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection.SpellGage {
    /// SG1 - DT3枚から1枚を得るセル
    struct Draw1Top3: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "SG1:DT3枚から1枚を得る"
        let titleColor: UIColor = .blue

        /// 非表示か
        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return player.reserves[.spellGage, default: 0] < 1
        }

        var disabledReason: String? {
            disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            // TODO: タップ時処理
        }
    }
}
