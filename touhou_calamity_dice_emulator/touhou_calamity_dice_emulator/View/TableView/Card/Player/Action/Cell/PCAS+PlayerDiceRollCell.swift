//
//  PCAS+PlayerDiceRollCell.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection {
    /// プレイヤーダイス選択セル
    struct PlayerDiceRoll: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "プレイヤーダイスを振る"
        let titleColor: UIColor = .orange

        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .playerDiceRoll
            })?.id != game.game.nowStep {
                return true
            }
            return false
        }

        var disabledReason: String? {
            disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            queueAnimation {
                $game.nextStep()
            }
        }
    }
}
