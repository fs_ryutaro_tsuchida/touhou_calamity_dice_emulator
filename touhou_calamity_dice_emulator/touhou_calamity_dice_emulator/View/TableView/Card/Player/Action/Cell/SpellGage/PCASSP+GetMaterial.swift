//
//  PCASSP+GetMaterial.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension PlayerCard.ActionSection.SpellGage {
    /// SG1 - 資材3を得るセル
    struct GetMaterial: PlayerCardActionCell {
        @Binding var game: GameViewModel

        let playerId: UUID
        let width: CGFloat

        let title: String = "SG1:資材3を得る"
        let titleColor: UIColor = .brown

        var isHidden: Bool {
            if game.game.players[id: playerId].playerTurnSteps.first(where: {
                $0.stepType == .action
            })?.id != game.game.nowStep {
                return true
            }
            return player.reserves[.spellGage, default: 0] < 1
        }

        var disabledReason: String? {
            if !SingleReserve.materials.contains(where: {
                player.reserves[$0, default: 0] <
                    player.maxReserves[$0, default: $0.defaultMaxValue]
            }) {
                return "既に資材獲得量が上限に達しています。"
            }

            return disabledReasonDefault
        }

        /// 警告タイトル
        @State var alertTitle: String = ""
        var alertTitleBinding: Binding<String> {
            $alertTitle
        }

        /// 警告表示中か
        @State var isPresentedAlert = false
        var isPresentedAlertBinding: Binding<Bool> {
            $isPresentedAlert
        }

        func onTap() {
            withAnimation {
                game.sheet.spellGage.getReserve.id = playerId
                game.sheet.viewType = .spellGage_getReserve
            }
        }
    }
}
