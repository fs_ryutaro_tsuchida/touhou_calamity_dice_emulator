//
//  SummonView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚画面
struct SummonView: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤーID
    var playerId: UUID { game.sheet.summon.id }

    var body: some View {
        SummonCard(game: $game, playerId: playerId)
            .navigationTitle("召喚")
            .navigationBarTitleDisplayMode(.inline)
    }
}
