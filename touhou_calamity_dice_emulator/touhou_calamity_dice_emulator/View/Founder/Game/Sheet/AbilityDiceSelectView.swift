//
//  AbilityDiceSelectView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// カード効果 - ダイス選択画面
struct AbilityDiceSelectView: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// 選択中のダイス出目
    @State private var selectDiceRoll: Int = -1

    var body: some View {
        GeometryReader { proxy in
            HStack {
                AbilityDiceSelect(
                    diceRolls: game.sheet.ability.diceRoll.previewDiceRolls,
                    selectDiceRoll: $selectDiceRoll
                )
            }
            .frame(width: proxy.size.width, alignment: .center)
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: trailingBarItem)
    }

    /// ナビゲーションバーの右側に表示するボタン
    @ViewBuilder var trailingBarItem: some View {
        if game.sheet.ability.diceRoll.previewDiceRolls.contains(selectDiceRoll) {
            Button {
                withAnimation {
                    game.sheet.ability.diceRoll.onResult(
                        game.sheet.ability.diceRoll.successDiceRolls.contains(selectDiceRoll)
                    )
                    game.sheet.isPresented = false
                }
            } label: {
                Text("決定")
            }
        } else {
            Text("決定")
                .foregroundColor(.gray)
        }
    }
}
