//
//  SpellGageGetReserveView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// SG - 資材獲得画面
struct SpellGageGetReserveView: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤーID
    var playerId: UUID { game.sheet.spellGage.getReserve.id }

    /// 選択中の資材
    @State private var selectReserves = [SingleReserve](repeating: .liquor, count: 3)

    /// 最大獲得量を下回っている資材一覧
    private var underMaterials: [SingleReserve] {
        SingleReserve.materials.filter { reserve in
            let nowValue = game.game.players[id: playerId].reserves[reserve, default: 0]
            let selectValue = selectReserves.filter { $0 == reserve }.count
            let maxValue = game.game.players[id: playerId]
                .maxReserves[reserve, default: reserve.defaultMaxValue]
            return (nowValue + selectValue) < maxValue
        }
    }

    /// 最大獲得量を上回っている資材一覧
    private var overMaterials: [SingleReserve] {
        SingleReserve.materials.filter { reserve in
            let nowValue = game.game.players[id: playerId].reserves[reserve, default: 0]
            let selectValue = selectReserves.filter { $0 == reserve }.count
            let maxValue = game.game.players[id: playerId]
                .maxReserves[reserve, default: reserve.defaultMaxValue]
            return (nowValue + selectValue) > maxValue
        }
    }

    /// 資材を獲得できるか
    private var canGetReserves: Bool {
        if game.game.players[id: playerId].reserves[.spellGage, default: 0] < 1 {
            return false
        }
        if !underMaterials.isEmpty, !overMaterials.isEmpty {
            return false
        }
        return true
    }

    /// 表示メッセージ
    private var message: String {
        if !overMaterials.isEmpty {
            if !underMaterials.isEmpty {
                return [
                    "一部資材が余計に取得されています。他の資材を取得してください",
                    "余計に取得している資材: \(overMaterials.map(\.name).joined(separator: "、"))",
                    "まだ取得できる資材: \(underMaterials.map(\.name).joined(separator: "、"))"
                ].joined(separator: "\n")
            } else {
                return "上限を超えた資材は切り捨てられます。"
            }
        }
        return ""
    }

    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 5) {
                UserReserveCollection(
                    player: $game.game.players[id: playerId],
                    reserves: SingleReserve.materials,
                    width: proxy.size.width
                )
                SpellGageGetReserve(selectReserves: $selectReserves)
                UserReserveCollection(
                    player: $game.game.players[id: playerId],
                    reserves: SingleReserve.materials,
                    additionReserves: selectReserves.reduce(into: [SingleReserve: Int]()) {
                        $0[$1, default: 0] += 1
                    },
                    width: proxy.size.width
                )
                if !message.isEmpty {
                    Text(message)
                        .lineLimit(nil)
                        .foregroundColor(canGetReserves ? .yellow : .red)
                }
                Spacer()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: trailingBarItems)
    }

    /// ナビゲーションバーの右側に表示するボタン
    @ViewBuilder var trailingBarItems: some View {
        if canGetReserves {
            Button {
                queueAnimation {
                    selectReserves.forEach { reserve in
                        game.game.players[id: playerId].reserves[reserve, default: 0] += 1
                    }
                    game.game.players[id: playerId].reserves[.spellGage, default: 0] -= 1
                    $game.finishAndRunAbilities { timing, cardPlayerId, _ in
                        if case let .useSelfReserve(reserves) = timing,
                           cardPlayerId == playerId,
                           reserves.contains(.spellGage) {
                            return true
                        }
                        if case let .useOpponentReserve(reserves) = timing,
                           cardPlayerId != playerId,
                           reserves.contains(.spellGage) {
                            return true
                        }
                        return false
                    }
                    game.sheet.isPresented = false
                }
            } label: {
                Text("決定")
            }
        } else {
            Text("決定")
                .foregroundColor(.gray)
        }
    }
}
