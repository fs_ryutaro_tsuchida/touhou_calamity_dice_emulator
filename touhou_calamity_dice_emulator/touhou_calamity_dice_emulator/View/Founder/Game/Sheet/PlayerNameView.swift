//
//  PlayerNameView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤー名入力画面
struct PlayerNameView: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel
    /// プレイヤー名
    @State var name: String
    /// プレイヤーID
    var playerId: UUID { game.sheet.playerName.id }
    /// エラー表示
    @State private var error: String = ""

    var body: some View {
        GeometryReader { proxy in
            ZStack(alignment: .top) {
                VStack {
                    TextField("名前", text: $name)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding(.horizontal, 5)
                    if !error.isEmpty {
                        Text(error)
                            .foregroundColor(.red)
                    }
                }
                .frame(width: proxy.size.width)
            }
        }
        .navigationTitle("プレイヤー名入力")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: Button("決定") {
            queueAnimation {
                if name.isEmpty {
                    error = "1文字以上入力してください。"
                } else {
                    error = ""
                    game.game.players[id: playerId].name = name
                    game.sheet.isPresented = false
                }
            }
        })
    }
}
