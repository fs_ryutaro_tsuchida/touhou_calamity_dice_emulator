//
//  SummonCostView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚コスト画面
struct SummonCostView: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel

    /// プレイヤーID
    var playerId: UUID
    /// プレイヤー
    var player: Player {
        game.game.players[id: playerId]
    }

    /// カードID
    var cardId: UUID
    /// カード
    var card: GameModel.GameCard {
        game.game.cards[id: cardId]
    }

    /// 表示中か
    @Binding var isActive: Bool

    /// 選択中の複合資材一覧
    @State var selectMultiReserves: [Int: MultiReserve] = [:]

    /// 選択中の複合資材
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - default: 初期値
    func selectMultiReserve(
        index: Int,
        default: MultiReserve = .anyReserve
    ) -> MultiReserve {
        selectMultiReserves[index, default: `default`]
    }

    /// 選択中の資材一覧
    @State var selectReservesInAbilities: [Int: [MultiReserve: [Int: SingleReserve]]] = [:]

    /// 選択中の資材一覧
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - default: 初期値
    func selectReservesInAbility(
        index: Int,
        default: [MultiReserve: [Int: SingleReserve]] = [:]
    ) -> [MultiReserve: [Int: SingleReserve]] {
        selectReservesInAbilities[index, default: `default`]
    }

    /// 選択中の資材一覧
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - multiReserve: 複合資材
    ///   - default: 初期値
    func selectReservesInMultiReserve(
        index: Int,
        multiReserve: MultiReserve,
        default: [Int: SingleReserve] = [:]
    ) -> [Int: SingleReserve] {
        selectReservesInAbility(index: index)[multiReserve, default: `default`]
    }

    /// 選択中の資材
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - multiReserve: 複合資材
    ///   - columnIndex: 列のインデックス
    ///   - default: 初期値
    func selectReserveInColumn(
        index: Int,
        multiReserve: MultiReserve,
        columnIndex: Int,
        default: SingleReserve = .liquor
    ) -> SingleReserve {
        selectReservesInMultiReserve(
            index: index,
            multiReserve: multiReserve
        )[columnIndex, default: `default`]
    }

    /// 召喚コストを変動させる能力一覧
    var abilities: [Int: ManipulateSummonCostAbilityProtocol] {
        var abilities: [ManipulateSummonCostAbilityProtocol] = []
        game.game.cards.forEach { abilityCard in
            guard let abilityPlayerId = abilityCard.fieldPlayerId(game.game) else {
                return
            }
            abilityCard.card.nestAbilities
                .mapOf(type: ManipulateSummonCostAbilityProtocol.self)
                .filter { ability in
                    ability.isTarget(
                        $game,
                        playerId: playerId,
                        card: card,
                        fromPlayerId: abilityPlayerId
                    )
                }.forEach { ability in
                    abilities.append(ability)
                }
            abilityCard.additionalAbilities.compactMap { ability
                -> (AdditionalAbility, ManipulateSummonCostAbilityProtocol)? in
                guard let costAbility = ability.ability(game)
                    as? ManipulateSummonCostAbilityProtocol
                else {
                    return nil
                }
                return (ability, costAbility)
            }.filter { additionalAbility, ability in
                ability.isTarget(
                    $game,
                    playerId: playerId,
                    card: card,
                    fromPlayerId: additionalAbility.fromPlayerId
                )
            }.forEach { _, ability in
                abilities.append(ability)
            }
        }
        abilities.sort { first, second in
            let firstType = first.type($game, card: card)
            let secondType = second.type($game, card: card)
            if firstType != secondType {
                return firstType > secondType
            }
            return true
        }
        return abilities.enumerated().reduce(
            into: [Int: ManipulateSummonCostAbilityProtocol]()
        ) {
            $0[$1.offset] = $1.element
        }
    }

    /// 初期必要コスト
    var needDefalutCost: [SingleReserve: Int] {
        card.card.costs
    }

    /// 資材の合計
    /// - Parameter index: 能力のインデックス
    func sumSelectReservesInAbility(index: Int) -> [SingleReserve: Int] {
        guard let ability = abilities[index] else {
            return needDefalutCost
        }
        let signum = ability.type($game, card: card).signum
        var reserves = sumSelectReservesInAbility(index: index - 1)
        let reservesInMultiReserve = selectReservesInMultiReserve(
            index: index,
            multiReserve: selectMultiReserve(index: index)
        )
        reservesInMultiReserve.values.forEach { reserve in
            reserves[reserve, default: 0] += signum
        }
        return reserves
    }

    /// 選択可能な資材一覧
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - multiReserve: 複合資材
    ///   - columnIndex: 列のインデックス
    func filterReservesInColumn(
        index: Int,
        multiReserve: MultiReserve,
        columnIndex: Int
    ) -> [SingleReserve] {
        var reserves = multiReserve.reserves
        guard let ability = abilities[index] else {
            return reserves
        }
        let prevSumSelectReserves = sumSelectReservesInAbility(index: index - 1)
        let selectReservesOnColumns = selectReservesInMultiReserve(
            index: index,
            multiReserve: multiReserve
        )
        let abilityType = ability.type($game, card: card)

        var sumPrevSelectReserves = prevSumSelectReserves
        selectReservesOnColumns.filter {
            $0.key < columnIndex
        }.forEach {
            sumPrevSelectReserves[$0.value, default: 0] += abilityType.signum
        }

        if abilityType.needFilter {
            reserves.removeAll {
                sumPrevSelectReserves[$0, default: 0] <= 0
            }
        }
        return reserves
    }

    /// 選択可能な資材一覧
    /// - Parameters:
    ///   - index: 能力のインデックス
    ///   - multiReserve: 複合資材
    func filterReservesInMultiReserve(
        index: Int,
        multiReserve: MultiReserve
    ) -> [Int: [SingleReserve]] {
        guard let ability = abilities[index] else {
            return [:]
        }
        let multiReserves = ability.cost($game, card: card)
        let count = multiReserves[multiReserve, default: 0]
        return (0..<count).reduce(
            into: [Int: [SingleReserve]]()
        ) { reserves, columnIndex in
            reserves[columnIndex] = filterReservesInColumn(
                index: index,
                multiReserve: multiReserve,
                columnIndex: columnIndex
            )
        }
    }

    /// 選択可能な資材一覧
    /// - Parameters:
    ///   - index: 能力のインデックス
    func filterReservesInAbility(index: Int) -> [MultiReserve: [Int: [SingleReserve]]] {
        guard let ability = abilities[index] else {
            return [:]
        }
        let multiReserves = ability.cost($game, card: card)
        return multiReserves.reduce(
            into: [MultiReserve: [Int: [SingleReserve]]]()
        ) { reserves, element in
            reserves[element.key] = filterReservesInMultiReserve(
                index: index,
                multiReserve: element.key
            )
        }
    }

    /// 選択可能な資材一覧
    var filterReservesInAbilities: [Int: [MultiReserve: [Int: [SingleReserve]]]] {
        abilities.keys.sorted().reduce(
            into: [Int: [MultiReserve: [Int: [SingleReserve]]]]()
        ) { reserves, index in
            reserves[index] = filterReservesInAbility(index: index)
        }
    }

    /// 必要コスト
    var needCost: [SingleReserve: Int] {
        if let lastIndex = abilities.keys.max() {
            return sumSelectReservesInAbility(index: lastIndex).filter { $0.value > 0 }
        }
        return needDefalutCost
    }

    /// 実際に取り除くコスト
    /// - Note: 手札から、対象カードの分を引く
    var removeCost: [SingleReserve: Int] {
        var cost = needCost
        cost[.handCard, default: 0] += 1
        return cost
    }

    /// メッセージ表示用の必要コスト
    /// - Note: 対象カードを置くゲージの分を引く
    var previewNeedCost: [SingleReserve: Int] {
        var cost = removeCost
        cost[.gage, default: 0] += player.fieldCardIds.count + 1
        return cost
    }

    /// メッセージ表示用の不足コスト
    var previewNotEnoughCost: [SingleReserve: Int] {
        previewNeedCost.reduce(into: [SingleReserve: Int]()) {
            $0[$1.key] = $1.value - player.reserves[$1.key, default: 0]
        }.filter { $0.value > 0 }
    }

    /// エラー表示中か
    @State private var isPresentedError = false

    /// エラータイトル
    var errorTitle: String? {
        guard previewNotEnoughCost.isEmpty else {
            return "以下の資材が不足しています"
        }
        return nil
    }

    /// エラーメッセージ
    var errorMessage: String? {
        guard previewNotEnoughCost.isEmpty else {
            return previewNotEnoughCost.sorted {
                $0.key.id < $1.key.id
            }.compactMap {
                "\($0.key.name) \($0.value)"
            }.joined(separator: "\n")
        }
        return nil
    }

    var body: some View {
        SummonCostAbility(
            game: $game,
            playerId: playerId,
            card: card,
            abilities: abilities,
            selectMultiReserves: $selectMultiReserves,
            selectReservesInAbilities: .init(get: {
                selectReservesInAbilities
            }, set: { reserves in
                selectReservesInAbilities = reserves
                checkSelectableReserves()
            }),
            filterReservesInAbilities: filterReservesInAbilities,
            removeCost: removeCost
        )
        .navigationTitle("召喚: \(card.card.name)")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: trailingBarItems)
        .onAppear(perform: initialize)
    }

    /// ナビゲーションバーの右側に表示するボタン
    @ViewBuilder var trailingBarItems: some View {
        if let title = errorTitle {
            Button {
                isPresentedError = true
            } label: {
                Text("決定")
                    .foregroundColor(.gray)
            }
            .alert(
                title,
                isPresented: $isPresentedError,
                actions: {
                    Button("OK") {}
                }, message: {
                    Text(errorMessage ?? "")
                }
            )
        } else {
            Button {
                withAnimation {
                    game.game.players[id: playerId].fieldCardIds.append(cardId)
                    game.game.cards[id: cardId].isShowAbilities = false
                    game.game.cards[id: cardId].isUserOperatorOnShowAbilities = false
                    removeCost.forEach {
                        game.game.players[id: playerId].reserves[$0, default: 0] -= $1
                    }
                    $game.finishAndRunAbilities { timing, _, abilityCardId in
                        if case .summon = timing, abilityCardId == cardId {
                            return true
                        }
                        return false
                    }
                    isActive = false
                    game.sheet.isPresented = false
                }
            } label: {
                Text("決定")
            }
        }
    }

    /// 初期化処理
    func initialize() {
        abilities.sorted {
            $0.key < $1.key
        }.forEach { index, ability in
            let multiReserves = ability.cost($game, card: card)
            multiReserves.sorted {
                $0.key < $1.key
            }.forEach { multiReserve, count in
                (0..<count).forEach { columnIndex in
                    selectReservesInAbilities[index, default: [:]][
                        multiReserve,
                        default: [:]
                    ][columnIndex] = multiReserve.reserves.first ?? .liquor
                }
            }
            selectMultiReserves[index] = multiReserves.keys.min() ?? .anyReserve
        }
        checkSelectableReserves()
    }

    /// 選択可能な資材か確認する
    func checkSelectableReserves() {
        abilities.sorted {
            $0.key < $1.key
        }.forEach { index, ability in
            let multiReserves = ability.cost($game, card: card)

            multiReserves.forEach { multiReserve, count in
                (0..<count).forEach { columnIndex in
                    let selectReserve = selectReserveInColumn(
                        index: index,
                        multiReserve: multiReserve,
                        columnIndex: columnIndex
                    )
                    let filterReserves = filterReservesInColumn(
                        index: index,
                        multiReserve: multiReserve,
                        columnIndex: columnIndex
                    )

                    if !filterReserves.contains(selectReserve),
                       let newReserve = filterReserves.first {
                        selectReservesInAbilities[index, default: [:]][
                            multiReserve,
                            default: [:]
                        ][columnIndex] = newReserve
                    }
                }
            }

            let selectMultiReserve = selectMultiReserve(index: index)
            let selectReserveInMultiReserves = selectReservesInAbility(index: index)

            let selectableMultiReserves = selectReserveInMultiReserves
                .filter { multiReserve, selectReserves in
                    selectReserves.contains { columnIndex, reserve in
                        filterReservesInColumn(
                            index: index,
                            multiReserve: multiReserve,
                            columnIndex: columnIndex
                        ).contains(reserve)
                    }
                }.keys

            if !selectableMultiReserves.contains(selectMultiReserve),
               let newMultiReserve = selectableMultiReserves.first {
                selectMultiReserves[index] = newMultiReserve
            }
        }
    }
}
