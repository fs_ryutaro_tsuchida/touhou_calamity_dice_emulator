//
//  GameViewModel.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ゲーム表示画面の表示データ
struct GameViewModel {
    /// ゲームデータ
    var game: GameModel

    /// シート表示データ
    var sheet = GameSheetModel()

    /// 選択中のプレイヤーID
    var nowSelectPlayerId = UUID()
}

extension Binding where Value == GameViewModel {
    /// 能力を一括で実行する
    /// - Parameters:
    ///   - isMatch: 能力の条件
    ///   - timing: 発動タイミング
    ///   - playerId: プレイヤーのID
    ///   - cardId: カードのID
    func runAbilities(
        isMatch: @escaping (
            _ timing: AbilityTiming,
            _ playerId: UUID,
            _ cardId: UUID?
        ) -> Bool
    ) {
        wrappedValue.game.players.forEach { player in
            // プレイヤーに追加されている能力
            player.additionalAbilities
                .compactMap { additionalAbility
                    -> (AdditionalAbility, AutoTriggerAbility)? in
                    if let ability = additionalAbility
                        .ability(wrappedValue) as? AutoTriggerAbility {
                        return (additionalAbility, ability)
                    }
                    return nil
                }.filter { _, ability in
                    isMatch(ability.timing, player.id, nil)
                }.forEach { additionalAbility, ability in
                    ability.run(
                        self,
                        fromPlayerId: player.id,
                        fromCardId: nil,
                        traceCardId: additionalAbility.traceCardId
                    ) {
                        wrappedValue.game.players[id: player.id]
                            .additionalAbilities.remove(id: additionalAbility.id)
                    }
                }
        }
        wrappedValue.game.cards.forEach { card in
            guard let playerId = card.fieldPlayerId(wrappedValue.game) else {
                return
            }
            // カードに追加されている能力
            card.additionalAbilities
                .compactMap { additionalAbility
                    -> (AdditionalAbility, AutoTriggerAbility)? in
                    if let ability = additionalAbility
                        .ability(wrappedValue) as? AutoTriggerAbility {
                        return (additionalAbility, ability)
                    }
                    return nil
                }.filter { _, ability in
                    isMatch(ability.timing, playerId, card.id)
                }.forEach { additionalAbility, ability in
                    ability.run(
                        self,
                        fromPlayerId: playerId,
                        fromCardId: card.id,
                        traceCardId: additionalAbility.traceCardId
                    ) {
                        wrappedValue.game.cards[id: card.id]
                            .additionalAbilities.remove(id: additionalAbility.id)
                    }
                }
            // カードの元々の能力
            card.card.abilities.compactMap {
                $0 as? AutoTriggerAbility
            }.filter { ability in
                isMatch(ability.timing, playerId, card.id)
            }.forEach { ability in
                ability.run(
                    self,
                    fromPlayerId: playerId,
                    fromCardId: card.id,
                    traceCardId: card.id
                ) {
                    // 何もしない
                }
            }
        }
    }

    /// 能力を一括で終了する
    /// - Parameters:
    ///   - isMatch: 能力の条件
    ///   - timing: 終了タイミング
    ///   - playerId: プレイヤーのID
    ///   - cardId: カードのID
    func finishAbilities(
        isMatch: @escaping (
            AbilityTiming?,
            _ playerId: UUID,
            _ cardId: UUID?
        ) -> Bool
    ) {
        wrappedValue.game.players.forEach { player in
            // プレイヤーに追加されている能力
            player.additionalAbilities.filter { ability in
                isMatch(ability.finishTiming, player.id, nil)
            }.forEach { ability in
                wrappedValue.game.players[id: player.id]
                    .additionalAbilities.remove(id: ability.id)
            }
        }
        wrappedValue.game.cards.forEach { card in
            guard let playerId = card.fieldPlayerId(wrappedValue.game) else {
                return
            }
            // カードに追加されている能力
            card.additionalAbilities.filter { ability in
                isMatch(ability.finishTiming, playerId, card.id)
            }.forEach { ability in
                wrappedValue.game.cards[id: card.id]
                    .additionalAbilities.remove(id: ability.id)
            }
        }
    }

    /// 能力を一括で終了/実行する
    /// - Parameters:
    ///   - isMatch: 能力の条件
    ///   - timing: タイミング
    ///   - playerId: プレイヤーのID
    ///   - cardId: カードのID
    func finishAndRunAbilities(
        isMatch: @escaping (
            AbilityTiming?,
            _ playerId: UUID,
            _ cardId: UUID?
        ) -> Bool
    ) {
        finishAbilities(isMatch: isMatch)
        runAbilities(isMatch: isMatch)
    }

    /// 次のステップに進める
    func nextStep() {
        let turn = wrappedValue.game.turns[id: wrappedValue.game.nowTurn]
        switch turn.type {
        case .roundStart:
            wrappedValue.game.roundStartSteps[id: wrappedValue.game.nowStep]
                .stepType.nextStep(self)
        case .player:
            wrappedValue.game.players[id: turn.id].nextStep(self)
        case .roundFinish:
            wrappedValue.game.roundFinishSteps[id: wrappedValue.game.nowStep]
                .stepType.nextStep(self)
        }
    }

    /// 可能なら次のステップに進める
    /// - Parameter deadline: 実行タイミング
    func nextStepIfCan(deadline: DispatchTime = .now()) {
        let turn = wrappedValue.game.turns[id: wrappedValue.game.nowTurn]
        switch turn.type {
        case .roundStart:
            wrappedValue.game.roundStartSteps[id: wrappedValue.game.nowStep]
                .stepType.nextStepIfCan(self, deadline: deadline)
        case .player:
            wrappedValue.game.players[id: turn.id].nextStepIfCan(self, deadline: deadline)
        case .roundFinish:
            wrappedValue.game.roundFinishSteps[id: wrappedValue.game.nowStep]
                .stepType.nextStepIfCan(self, deadline: deadline)
        }
    }

    /// 次のターンに移動
    func nextTurn() {
        Turn.nextTurn(self)
    }

    /// シート表示拡張
    var sheetModifier: GameSheetModel.Modifier {
        `self`.sheet.modifier(self)
    }
}
