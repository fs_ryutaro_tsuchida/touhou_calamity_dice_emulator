//
//  GameView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ゲーム表示画面
struct GameView: View {
    /// ゲーム表示画面の表示データ
    @State var model: GameViewModel
    /// 初期表示か
    @State var isFirst = true

    var body: some View {
        GeometryReader { proxy in
            VStack(spacing: 5) {
                roundSegment
                HStack {
                    roundStartSegment
                    playerTurnSegment
                    roundFinishSegment
                }
                playerSegment
                UserInfoCollection(game: $model)
            }
            .frame(
                width: proxy.size.width,
                height: proxy.size.height
            )
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(trailing: trailingBarItems)
        .modifier($model.sheetModifier)
        .onAppear(perform: setupIfFirst)
    }

    /// ナビゲーションバーの右側に表示するボタン
    private var trailingBarItems: some View {
        Menu {
            Menu {
                ForEach(model.game.players) { player in
                    Menu {
                        Button {
                            withAnimation {
                                model.sheet.playerName.id = player.id
                                model.sheet.viewType = .playerName
                            }
                        } label: {
                            Text("名前変更")
                        }
                    } label: {
                        Text(player.name)
                    }
                }
            } label: {
                Text("プレイヤー設定")
            }
        } label: {
            Image(systemName: "ellipsis.circle")
                .renderingMode(.template)
                .foregroundColor(.init(UIColor.link))
        }
    }

    /// ラウンド表示用セグメント
    private var roundSegment: some View {
        Picker("", selection: $model.game.nowTurn) {
            ForEach(model.game.turns) { turn in
                Text(turn.name(model))
                    .id(turn.id)
            }
        }
        .pickerStyle(.segmented)
        .disabled(true)
        .padding(.horizontal, 5)
    }

    /// ラウンド開始ステップ表示用セグメント
    private var roundStartSegment: some View {
        ZStack {
            if model.game.turns.first(id: model.game.nowTurn)?.type == .roundStart {
                Picker("", selection: $model.game.nowStep) {
                    ForEach(model.game.roundStartSteps) { step in
                        Text(step.stepType.name)
                            .id(step.id)
                    }
                }
                .pickerStyle(.segmented)
                .disabled(true)
                .padding(.horizontal, 5)
            }
        }
    }

    /// プレイヤーターンステップ表示用セグメント
    private var playerTurnSegment: some View {
        ZStack {
            if let player = model.game.players.first(id: model.game.nowTurn) {
                Picker("", selection: $model.game.nowStep) {
                    ForEach(player.playerTurnSteps) { step in
                        Text(step.stepType.name)
                            .id(step.id)
                    }
                }
                .pickerStyle(.segmented)
                .disabled(true)
                .padding(.horizontal, 5)
            }
        }
    }

    /// ラウンド終了ステップ表示用セグメント
    private var roundFinishSegment: some View {
        ZStack {
            if model.game.turns.first(id: model.game.nowTurn)?.type == .roundFinish {
                Picker("", selection: $model.game.nowStep) {
                    ForEach(model.game.roundFinishSteps) { step in
                        Text(step.stepType.name)
                            .id(step.id)
                    }
                }
                .pickerStyle(.segmented)
                .disabled(true)
                .padding(.horizontal, 5)
            }
        }
    }

    /// プレイヤー表示用セグメント
    private var playerSegment: some View {
        Picker("", selection: $model.nowSelectPlayerId.animation()) {
            ForEach(model.game.players) { player in
                Text(player.name)
                    .id(player.id)
            }
        }
        .pickerStyle(.segmented)
        .padding(.horizontal, 5)
    }

    /// 初期設定を行う
    func setupIfFirst() {
        guard isFirst else {
            return
        }
        isFirst = false

        model.game.nowStep = model.game.roundStartSteps.first {
            $0.stepType == .roundStart
        }?.id ?? .init()
        model.nowSelectPlayerId = model.game.players.first?.id ?? .init()
        $model.nextTurn()
    }
}
