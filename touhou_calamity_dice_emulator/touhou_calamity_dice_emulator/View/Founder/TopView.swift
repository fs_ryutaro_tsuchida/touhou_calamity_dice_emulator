//
//  TopView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// トップ画面
struct TopView: View {
    var body: some View {
        GeometryReader { proxy in
            VStack {
                NavigationLink {
                    GameView(model: GameViewModel(
                        game: GameModel(players: [
                            Player(name: "A"),
                            Player(name: "B"),
                            Player(name: "C")
                        ])
                    ))
                } label: {
                    Text("Battle Start")
                        .font(Font.system(size: 20))
                        .foregroundColor(.red)
                }
            }
            .frame(
                width: proxy.size.width,
                height: proxy.size.height
            )
        }
        .navigationTitle("トップ")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarHidden(true)
    }
}
