//
//  NumberBadgeIcon.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 数字バッジ表示
struct NumberBadgeIcon: View {
    /// 値
    /// - Note: 0の場合は表示しない
    let value: Int

    /// 値
    var valueStr: String {
        if value <= 0 {
            return ""
        } else if value > 50 {
            return "exclamationmark"
        } else {
            return "\(value)"
        }
    }

    var body: some View {
        TextBadgeIcon(value: valueStr)
    }
}
