//
//  ModalSheetView.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// モーダル表示
struct ModalSheetView<Sheet: View>: UIViewControllerRepresentable {
    /// 表示しているか
    @Binding var isPresented: Bool
    /// 対応している表示サイズ
    var detents: [UISheetPresentationController.Detent]
    /// 表示終了時処理
    var onDismiss: () -> Void = {}
    /// 表示する画面
    @ViewBuilder var content: () -> Sheet

    func makeUIViewController(context: Context) -> UIViewController {
        UIViewController()
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        if isPresented {
            let sheetController = ModalSheetHostingController(rootView: content())
            sheetController.detents = detents
            sheetController.presentationController?.delegate = context.coordinator
            uiViewController.present(sheetController, animated: true)
        } else {
            uiViewController.dismiss(animated: true, completion: onDismiss)
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UISheetPresentationControllerDelegate {
        /// モーダル表示
        var parent: ModalSheetView

        /// :nodoc:
        init(_ parent: ModalSheetView) {
            self.parent = parent
        }

        /// :nodoc:
        func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
            parent.isPresented = false
        }
    }

    /// モーダル表示用ViewController
    class ModalSheetHostingController<Content: View>: UIHostingController<Content> {
        /// 対応している表示サイズ
        var detents = [UISheetPresentationController.Detent]()

        /// :nodoc:
        override func viewDidLoad() {
            super.viewDidLoad()

            guard let sheet = sheetPresentationController else {
                return
            }
            sheet.detents = detents
            sheet.prefersGrabberVisible = true
        }
    }
}

extension View {
    /// モーダル表示拡張
    /// - Parameters:
    ///   - isPresented: 表示しているか
    ///   - detents: 対応している表示サイズ
    ///   - onDismiss: 表示終了時処理
    ///   - content: 表示する画面
    /// - Returns: モーダル表示拡張
    func sheet<Sheet: View>(
        isPresented: Binding<Bool>,
        detents: [UISheetPresentationController.Detent],
        onDismiss: @escaping () -> Void = {},
        @ViewBuilder content: @escaping () -> Sheet
    ) -> some View {
        background(
            ModalSheetView<Sheet>(
                isPresented: isPresented,
                detents: detents,
                onDismiss: onDismiss,
                content: content
            )
        )
    }
}
