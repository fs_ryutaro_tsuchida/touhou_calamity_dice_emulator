//
//  TextBadgeIcon.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 文字列バッジ表示
struct TextBadgeIcon: View {
    /// 値
    /// - Note: 空文字の場合は表示しない
    let value: String

    var body: some View {
        if value.isEmpty {
            EmptyView()
        } else {
            Image(systemName: "\(value).circle.fill")
                .renderingMode(.template)
                .foregroundColor(.red)
        }
    }
}
