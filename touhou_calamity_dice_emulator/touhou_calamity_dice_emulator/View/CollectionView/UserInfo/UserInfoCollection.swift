//
//  UserInfoCollection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ユーザー情報一覧表示
struct UserInfoCollection: View {
    /// ゲーム表示画面の表示データ
    @Binding var game: GameViewModel

    var body: some View {
        GeometryReader { proxy in
            TabView(selection: $game.nowSelectPlayerId) {
                ForEach($game.game.players) { player in
                    VStack(spacing: 0) {
                        UserReserveCollection(
                            player: player,
                            reserves: SingleReserve.userReserves,
                            width: proxy.size.width
                        )
                        Spacer(minLength: 10)
                        PlayerCard(game: $game, playerId: player.id)
                    }
                    .tag(player.wrappedValue.id)
                    .frame(width: proxy.size.width)
                }
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
        }
    }
}
