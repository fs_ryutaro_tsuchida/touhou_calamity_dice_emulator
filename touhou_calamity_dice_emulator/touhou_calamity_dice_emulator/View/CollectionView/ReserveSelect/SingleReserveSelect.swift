//
//  SingleReserveSelect.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 単独資材選択表示
struct SingleReserveSelect: View {
    /// 資材一覧
    let reserves: [SingleReserve]
    /// 選択中の資材
    @Binding var selectReserve: SingleReserve
    /// 選択している資材の枠の色
    let selectFrameColor: UIColor

    /// アニメーション用のネームスペース
    @Namespace private var nameSpace

    var body: some View {
        HStack(spacing: 8) {
            ForEach(reserves) { reserve in
                Button {
                    queueAnimation {
                        selectReserve = reserve
                    }
                } label: {
                    if let icon = reserve.icon,
                       let color = reserve.color {
                        Image(uiImage: icon)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 50, height: 50)
                            .foregroundColor(Color(uiColor: color))
                            .padding(5)
                            .background {
                                if reserve == selectReserve {
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color(selectFrameColor), lineWidth: 1)
                                        .matchedGeometryEffect(id: "selectFrame", in: nameSpace)
                                }
                            }
                    }
                }
            }
        }
        .padding(2)
        .background(
            Rectangle()
                .fill(Color(.secondarySystemBackground))
                .cornerRadius(12)
        )
    }
}
