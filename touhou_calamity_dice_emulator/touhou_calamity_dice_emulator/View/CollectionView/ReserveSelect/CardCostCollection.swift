//
//  CardCostCollection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// カードコスト一覧表示
struct CardCostCollection: View {
    /// カードコスト
    let costs: [SingleReserve: Int]

    /// 資材一覧
    /// - Note: 表示用
    var reserves: [SingleReserve] {
        costs.sorted {
            $0.key.id < $1.key.id
        }.reduce(into: [SingleReserve]()) {
            $0.append(contentsOf: [SingleReserve](
                repeating: $1.key,
                count: $1.value
            ))
        }
    }

    var body: some View {
        if reserves.isEmpty {
            EmptyView()
        } else {
            HStack(spacing: 8) {
                ForEach(reserves) {
                    if let icon = $0.icon,
                       let color = $0.color {
                        Image(uiImage: icon)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .foregroundColor(Color(uiColor: color))
                    } else {
                        EmptyView()
                    }
                }
            }
            .padding(6)
            .background(
                Rectangle()
                    .fill(Color(uiColor: .secondarySystemBackground))
                    .cornerRadius(6)
            )
        }
    }
}
