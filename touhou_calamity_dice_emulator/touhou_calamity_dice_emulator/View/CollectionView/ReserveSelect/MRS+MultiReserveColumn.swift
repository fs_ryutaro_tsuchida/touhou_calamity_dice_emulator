//
//  MRS+MultiReserveColumn.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension MultiReserveSelect {
    /// 複合資材選択の列表示
    struct MultiReserveColumn: View {
        /// 選択可能な資材一覧
        let filterReserves: [Int: [SingleReserve]]
        /// 選択中の資材一覧
        @Binding var selectReserves: [Int: SingleReserve]
        /// 選択している資材の枠の色
        let selectFrameColor: UIColor

        var body: some View {
            VStack(spacing: 5) {
                ForEach(filterReserves.sorted {
                    $0.key < $1.key
                }, id: \.key) { index, reserves in
                    if !reserves.isEmpty {
                        SingleReserveSelect(
                            reserves: reserves,
                            selectReserve: .init(get: {
                                selectReserves[index, default: .liquor]
                            }, set: { reserve in
                                selectReserves[index] = reserve
                            }),
                            selectFrameColor: selectFrameColor
                        )
                    }
                }
            }
            .padding(2)
            .background(
                Rectangle()
                    .fill(Color(uiColor: .systemBackground))
                    .cornerRadius(12)
            )
        }
    }
}
