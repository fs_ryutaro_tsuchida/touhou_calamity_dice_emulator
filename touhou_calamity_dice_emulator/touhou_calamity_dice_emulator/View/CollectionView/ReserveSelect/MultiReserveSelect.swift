//
//  MultiReserveSelect.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 複合資材選択表示
struct MultiReserveSelect: View {
    /// 複合資材一覧
    let multiReserves: [MultiReserve]
    /// 選択中の複合資材
    @Binding var selectMultiReserve: MultiReserve
    /// 複合資材毎の選択資材一覧
    @Binding var selectReservesInMultiReserves: [MultiReserve: [Int: SingleReserve]]
    /// 選択可能な資材一覧
    let filterReservesInMultiReserves: [MultiReserve: [Int: [SingleReserve]]]

    /// アニメーション用のネームスペース
    @Namespace private var nameSpace

    var body: some View {
        HStack(spacing: 5) {
            ForEach(multiReserves.sorted()) { multiReserve in
                if filterReservesInMultiReserves[multiReserve, default: [:]]
                    .contains(where: { _, reserves in
                        !reserves.isEmpty
                    }) {
                    MultiReserveColumn(
                        filterReserves: filterReservesInMultiReserves[multiReserve, default: [:]],
                        selectReserves: .init(get: {
                            selectReservesInMultiReserves[multiReserve, default: [:]]
                        }, set: { reserves in
                            selectMultiReserve = multiReserve
                            selectReservesInMultiReserves[multiReserve] = reserves
                        }),
                        selectFrameColor: (selectMultiReserve == multiReserve)
                            ? .label
                            : .secondaryLabel
                    )
                    .padding(2)
                    .background {
                        if selectMultiReserve == multiReserve {
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color(.label), lineWidth: 1)
                                .matchedGeometryEffect(id: "selectFrame", in: nameSpace)
                        }
                    }
                }
            }
        }
        .padding(2)
        .background(
            Rectangle()
                .fill(Color(uiColor: .secondarySystemBackground))
                .cornerRadius(12)
        )
    }
}
