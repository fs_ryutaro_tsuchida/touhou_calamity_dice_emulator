//
//  UserReserveCollection.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ユーザーリザーブコレクション表示
struct UserReserveCollection: View {
    /// プレイヤー
    @Binding var player: Player
    /// プレイヤーのリザーブ
    var reserves: [SingleReserve]
    /// 追加のリザーブ
    var additionReserves: [SingleReserve: Int] = [:]

    /// 横幅
    var width: CGFloat

    var body: some View {
        LazyVGrid(
            columns: [GridItem].init(
                repeating: .init(
                    .flexible(
                        minimum: 0,
                        maximum: width
                    )
                ),
                count: 4
            ),
            spacing: 5
        ) {
            ForEach(reserves) { reserve in
                HStack(spacing: 5) {
                    if let icon = reserve.icon,
                       let color = reserve.color {
                        Image(uiImage: icon)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30, height: 30)
                            .foregroundColor(Color(color))
                    }
                    HStack(spacing: 0) {
                        Text("\(self.nowReserve(reserve))")
                            .foregroundColor(nowReserveColor(reserve))
                        Text("/")
                        Text("\(self.maxReserve(reserve))")
                            .foregroundColor(maxReserveColor(reserve))
                    }
                }
            }
        }
    }

    /// 現在の資材獲得数
    /// - Parameter reserve: 資材種別
    /// - Returns: 現在の資材獲得数
    func nowReserve(_ reserve: SingleReserve) -> Int {
        player.reserves[reserve, default: 0] + additionReserves[reserve, default: 0]
    }

    /// 資材の最大獲得数
    /// - Parameter reserve: 資材種別
    /// - Returns: 資材の最大獲得数
    func maxReserve(_ reserve: SingleReserve) -> Int {
        player.maxReserves[reserve, default: reserve.defaultMaxValue]
    }

    /// リザーブ獲得量の表示色
    /// - Parameter reserve: リザーブ種別
    /// - Returns: 表示色
    func nowReserveColor(_ reserve: SingleReserve) -> Color {
        let value = nowReserve(reserve)
        let maxValue = maxReserve(reserve)

        if value >= maxValue {
            return .red
        } else if value < 0 {
            return .red
        } else if value == 0 {
            return .gray
        } else if value == maxValue - 1 {
            return .orange
        } else {
            return .init(UIColor.label)
        }
    }

    /// リザーブ最大獲得量の表示色
    /// - Parameter reserve: リザーブ種別
    /// - Returns: 表示色
    func maxReserveColor(_ reserve: SingleReserve) -> Color {
        let maxValue = maxReserve(reserve)

        if maxValue > reserve.defaultMaxValue {
            return .blue
        } else if maxValue < reserve.defaultMaxValue {
            return .red
        } else {
            return .init(UIColor.label)
        }
    }
}
