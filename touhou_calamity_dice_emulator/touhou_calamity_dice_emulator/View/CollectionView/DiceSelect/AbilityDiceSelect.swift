//
//  AbilityDiceSelect.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// カード効果 - ダイス選択表示
struct AbilityDiceSelect: View {
    /// ダイスの出目一覧
    let diceRolls: [Int]
    /// 選択中のダイスの出目
    @Binding var selectDiceRoll: Int

    /// アニメーション用のネームスペース
    @Namespace private var nameSpace

    var body: some View {
        LazyVGrid(columns: [GridItem](repeating: .init(
            .flexible(minimum: 25, maximum: 35),
            spacing: 10
        ), count: 5)) {
            ForEach(diceRolls, id: \.self) { diceRoll in
                Button {
                    withAnimation {
                        selectDiceRoll = diceRoll
                    }
                } label: {
                    ZStack {
                        if diceRoll == selectDiceRoll {
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color(uiColor: .label), lineWidth: 1)
                                .frame(width: 30, height: 30)
                                .matchedGeometryEffect(id: "selectFrame", in: nameSpace)
                        }

                        Text("\(diceRoll)")
                    }
                    .frame(width: 30, height: 30, alignment: .center)
                }
            }
        }
        .frame(width: 200)
        .background(
            Rectangle()
                .fill(Color(uiColor: .secondarySystemBackground))
                .cornerRadius(12)
        )
    }
}
