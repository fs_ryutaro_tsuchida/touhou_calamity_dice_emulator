//
//  Sequence+mapOf.swift
//  touhou_calamity_dice_emulator
//

import Foundation

extension Sequence {
    /// 配列データの型絞り込み
    /// - Parameter type: 絞り込む型
    /// - Returns: 該当するデータの配列
    func mapOf<T>(type: T.Type) -> [T] {
        compactMap { $0 as? T }
    }
}
