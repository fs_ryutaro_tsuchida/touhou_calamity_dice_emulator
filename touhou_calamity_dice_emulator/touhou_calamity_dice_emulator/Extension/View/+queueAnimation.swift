//
//  +queueAnimation.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 指定したキューの中でアニメーションを実行する
/// - Parameters:
///   - animation: アニメーション種別
///   - queue: 実行するキュー
///   - body: アニメーション内容
func queueAnimation(
    _ animation: Animation? = .default,
    queue: DispatchQueue = .main,
    body: @escaping () -> Void
) {
    queue.async {
        withAnimation(animation, body)
    }
}
