//
//  Array+whereIdentifiable.swift
//  touhou_calamity_dice_emulator
//

import Foundation

extension Array where Element: Identifiable {
    /// IDが一致するデータ
    /// - Parameter id: 対象データのID
    subscript(id id: Element.ID) -> Element {
        get {
            first { $0.id == id }!
        }
        set {
            if let index = firstIndex(id: id) {
                self[index] = newValue
            } else {
                append(newValue)
            }
        }
    }

    /// IDが一致するデータ
    /// - Parameter id: 対象データのID
    /// - Returns: 対象データ
    func first(id: Element.ID) -> Element? {
        first { $0.id == id }
    }

    /// IDが一致するデータのインデックス
    /// - Parameter id: 対象データのID
    /// - Returns: インデックス
    func firstIndex(id: Element.ID) -> Index? {
        firstIndex { $0.id == id }
    }

    /// IDが一致するデータが含まれているか
    /// - Parameter id: 対象データのID
    /// - Returns: 含まれている:true
    func contains(id: Element.ID) -> Bool {
        contains { $0.id == id }
    }

    /// IDが一致するデータを削除する
    /// - Parameter id: 対象データのID
    mutating func remove(id: Element.ID) {
        if let index = firstIndex(id: id) {
            remove(at: index)
        }
    }
}
