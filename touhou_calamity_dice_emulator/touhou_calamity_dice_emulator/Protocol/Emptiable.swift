//
//  Emptiable.swift
//  touhou_calamity_dice_emulator
//

import CloudKit
import Foundation

/// 空の値か判定できるプロトコル
protocol Emptiable {
    /// 空の値
    static var empty: Self { get }

    /// 空の値か
    var isEmpty: Bool { get }
}

extension Emptiable {
    /// 空の場合に別の値を返す
    /// - Parameter then: 空の場合に返す値
    /// - Returns: 空でない場合は自身を返す
    func ifEmpty(_ then: Self) -> Self {
        isEmpty ? then : self
    }
}

extension String: Emptiable {
    /// 空の値
    static let empty = ""
}

extension Substring: Emptiable {
    /// 空の値
    static let empty = Substring("")
}

extension Array: Emptiable {
    /// 空の値
    static var empty: [Element] {
        []
    }
}

extension Dictionary: Emptiable {
    /// 空の値
    static var empty: [Key: Value] {
        [:]
    }
}

extension NSArray: Emptiable {
    /// 空の値
    static var empty: Self {
        Self()
    }

    /// 空の値か
    var isEmpty: Bool {
        // swiftlint:disable:next empty_count
        count == 0
    }
}

extension NSDictionary: Emptiable {
    /// 空の値
    static var empty: Self {
        Self()
    }

    /// 空の値か
    var isEmpty: Bool {
        // swiftlint:disable:next empty_count
        count == 0
    }
}
