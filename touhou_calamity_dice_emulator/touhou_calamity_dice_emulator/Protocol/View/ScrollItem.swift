//
//  ScrollItem.swift
//  touhou_calamity_dice_emulator
//

import Foundation
import SwiftUI

/// GeometryProxyが指定されていない場合に自動取得するScrollView
protocol ScrollItem: View {
    /// ScrollItemのbodyのView種別
    associatedtype ScrollItemBody: View

    /// スクロール方向
    var axis: Axis.Set { get }
    /// インジケータを表示するか
    var showsIndicators: Bool { get }

    /// ScrollItemのbodyを生成
    /// - Parameter proxy: GeometryProxy
    /// - Returns: ScrollItemのbody
    @ViewBuilder
    func scrollBody(_ proxy: GeometryProxy) -> ScrollItemBody
}

extension ScrollItem {
    var body: some View {
        GeometryReader { proxy in
            ScrollView(axis, showsIndicators: showsIndicators) {
                scrollBody(proxy)
            }
            .frame(
                width: axis == .vertical ? proxy.size.width : nil,
                height: axis == .horizontal ? proxy.size.height : nil
            )
        }
    }
}
