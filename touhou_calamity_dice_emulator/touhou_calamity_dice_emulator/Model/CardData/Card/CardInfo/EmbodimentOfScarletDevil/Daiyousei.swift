//
//  Daiyousei.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// 大妖精
    struct Daiyousei: CardInfoProtocol {
        let card = Card(
            cardId: .daiyousei,
            tribeId: .embodimentOfScaletDevil,
            name: "大妖精",
            shortName: "大妖精",
            abilities: [
                AutoTriggerAbility(
                    text: "【永続】SGを使った時、切れ端2を得る。",
                    timing: .useSelfReserve(
                        reserves: [.spellGage]
                    ),
                    runAbility: SelfCardAdditionalAbility(
                        additionalAbility: GetResourceAbility(
                            text: "切れ端2を得る。",
                            reserveChoices: [
                                .slip: 2
                            ]
                        ),
                        finishTiming: nil
                    )
                ),
                AutoTriggerAbility(
                    text: "CP-チルノ:SGを使った時、ダイスを振り奇数ならGを得る。",
                    timing: .useSelfReserve(
                        reserves: [.spellGage]
                    ),
                    runAbility: CouplingAbility(
                        text: "CP-チルノ:ダイスを振り奇数ならGを得る。",
                        couplingCardId: .cirno,
                        couplingAbility: SelfCardAdditionalAbility(
                            additionalAbility: DiceRollAbility(
                                text: "ダイスを振り奇数ならGを得る。",
                                successDiceRolls: [1, 3, 5, 7, 9],
                                previewDiceRolls: [Int](0...9),
                                diceRollAbility: GetResourceAbility(
                                    text: "Gを得る。",
                                    reserveChoices: [
                                        .gage: 1
                                    ]
                                )
                            ),
                            finishTiming: nil
                        )
                    )
                )
            ],
            costs: [
                .book: 2
            ]
        )

        let count: Int = 2
    }
}
