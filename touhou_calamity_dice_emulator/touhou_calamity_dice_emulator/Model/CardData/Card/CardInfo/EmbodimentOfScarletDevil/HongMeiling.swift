//
//  HongMeiling.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// 紅美鈴
    struct HongMeiling: CardInfoProtocol {
        var card = Card(
            cardId: .hongMeiling,
            tribeId: .embodimentOfScaletDevil,
            name: "紅美鈴",
            shortName: "美鈴",
            abilities: [
                GetResourceAbility(
                    text: "【単-起動】場の紅魔勢力のカードを1枚選ぶ、召喚Cを支払い召喚を発動する。",
                    reserveChoices: [:]
                ),
                DecreaseSummonCostAbility(
                    text: "【永続】紅魔勢力の召喚Cを1減らす。",
                    costs: [
                        .anyReserve: 1
                    ],
                    targetPlayer: .selfPlayer,
                    targetCard: .tribe(tribeId: .embodimentOfScaletDevil)
                ),
                GetResourceAbility(
                    text: "CP-咲夜:【受-起動】C・切れ端 対戦相手のカードの効果を受けない。",
                    reserveChoices: [:]
                )
            ],
            costs: [
                .power: 3
            ]
        )

        var count: Int = 2
    }
}
