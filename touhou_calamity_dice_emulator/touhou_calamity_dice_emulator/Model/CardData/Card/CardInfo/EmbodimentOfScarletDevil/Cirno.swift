//
//  Cirno.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// チルノ
    struct Cirno: CardInfoProtocol {
        let card = Card(
            cardId: .cirno,
            tribeId: .embodimentOfScaletDevil,
            name: "チルノ",
            shortName: "チルノ",
            abilities: [
                GetResourceAbility(
                    text: "【召喚】次の自分のT開始時まで全員、起動、受-起動、単-起動を発動することができない。",
                    reserveChoices: [:]
                ),
                GetResourceAbility(
                    text: "CP-大妖精:【起動】C・切れ端3 このカードの召喚を発動する。",
                    reserveChoices: [:]
                )
            ],
            costs: [:]
        )

        let count: Int = 2
    }
}
