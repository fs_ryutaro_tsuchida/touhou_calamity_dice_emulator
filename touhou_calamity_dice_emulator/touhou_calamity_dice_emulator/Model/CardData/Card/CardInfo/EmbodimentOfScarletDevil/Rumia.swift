//
//  Rumia.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// ルーミア
    struct Rumia: CardInfoProtocol {
        let card = Card(
            cardId: .rumia,
            tribeId: .embodimentOfScaletDevil,
            name: "ルーミア",
            shortName: "ルーミア",
            abilities: [
                AutoTriggerAbility(
                    text: "【召喚】このR、全員召喚できない。",
                    timing: .summon,
                    runAbility: PlayerAdditionalAbility(
                        text: "このR、全員召喚できない。",
                        additionalAbility: CannotSummonAbility(
                            text: "このR、召喚できない。"
                        ),
                        finishTiming: .roundStart,
                        targetPlayer: .multipleInAllPlayer,
                        isCancelIfFromCardRemove: true
                    )
                )
            ],
            costs: [
                .power: 1
            ]
        )

        let count: Int = 2
    }
}
