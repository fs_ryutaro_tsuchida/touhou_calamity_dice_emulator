//
//  CardInfoProtocol.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// カード情報
protocol CardInfoProtocol {
    /// カード
    var card: Card { get }
    /// 枚数
    var count: Int { get }
}

extension CardInfoProtocol {
    /// カード一覧
    var cards: [Card] {
        .init(repeating: card, count: count)
    }
}
