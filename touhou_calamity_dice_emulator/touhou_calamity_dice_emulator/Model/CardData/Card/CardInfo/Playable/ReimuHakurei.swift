//
//  ReimuHakurei.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// 博麗霊夢
    struct ReimuHakurei: CardInfoProtocol {
        let card = Card(
            cardId: .reimuHakurei,
            tribeId: .playable,
            name: "博麗霊夢",
            shortName: "霊夢",
            abilities: [
                GetResourceAbility(
                    text: "【受-起動】C・G2 対戦相手のカードの効果を受けない。",
                    reserveChoices: [:]
                ),
                GetResourceAbility(
                    text: "【永続】任意のタイミングで2度だけ異変/カードの効果を受けない。",
                    reserveChoices: [:]
                )
            ],
            costs: [
                .liquor: 1,
                .money: 1,
                .book: 1,
                .power: 1,
                .handCard: 1
            ]
        )

        let count: Int = 2
    }
}
