//
//  MarisaKirisame.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// 霧雨魔理沙
    struct MarisaKirisame: CardInfoProtocol {
        let card = Card(
            cardId: .marisaKirisame,
            tribeId: .playable,
            name: "霧雨魔理沙",
            shortName: "魔理沙",
            abilities: [
                AutoTriggerAbility(
                    text: "【永続】R終了時、資材と切れ端2を得る。",
                    timing: .roundFinish,
                    runAbility: SelfCardAdditionalAbility(
                        additionalAbility: MultiRunnableAbility(
                            text: "資材と切れ端2を得る。",
                            abilities: [
                                GetResourceAbility(
                                    text: "資材を得る。",
                                    reserveChoices: SingleReserve.materials.reduce(
                                        into: [SingleReserve: Int]()
                                    ) {
                                        $0[$1] = 1
                                    }
                                ),
                                GetResourceAbility(
                                    text: "切れ端2を得る。",
                                    reserveChoices: [.slip: 2]
                                )
                            ]
                        ),
                        finishTiming: nil
                    )
                ),
                GetResourceAbility(
                    text: "CP-パチュリー:【起動】C・カード 対戦相手はカード2を失う。",
                    reserveChoices: [:]
                ),
                GetResourceAbility(
                    text: "CP-アリス:【起動】C・G 対戦相手はG2を失う。",
                    reserveChoices: [:]
                ),
                GetResourceAbility(
                    text: "CP-にとり:【起動】C・資材 対戦相手は資材2と切れ端2を失う。",
                    reserveChoices: [:]
                )
            ],
            costs: [
                .liquor: 2,
                .book: 2,
                .handCard: 1
            ]
        )

        let count: Int = 2
    }
}
