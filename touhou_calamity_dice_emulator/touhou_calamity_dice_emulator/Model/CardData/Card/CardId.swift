//
//  CardId.swift
//  touhou_calamity_dice_emulator
//

import Foundation

extension Card {
    /// カードID
    enum CardId: String, CaseIterable, Comparable {
        static func < (lhs: Card.CardId, rhs: Card.CardId) -> Bool {
            // swiftlint:disable:next force_unwrapping
            allCases.firstIndex(of: lhs)! < allCases.firstIndex(of: rhs)!
        }

        // playable
        /// 霊夢博麗
        case reimuHakurei = "ReimuHakurei"
        /// 霧雨魔理沙
        case marisaKirisame = "MarisaKirisame"
        // embodimentOfScarletDevil
        /// ルーミア
        case rumia = "Rumia"
        /// 大妖精
        case daiyousei = "Daiyousei"
        /// チルノ
        case cirno = "Cirno"
        /// 紅美鈴
        case hongMeiling = "HongMeiling"
        /// パチュリー・ノーレッジ
        case patchouliKnowledge = "PatchouliKnowledge"
        /// 十六夜咲夜
        case sakuyaIzayoi = "SakuyaIzayoi"
        /// レミリア・スカーレット
        case remiliaScarlet = "RemiliaScarlet"
        /// フランドール・スカーレット
        case flandreScarlet = "FlandreScarlet"
        // perfectCherryBlossom
        /// レティ・ホワイトロック
        case lettyWhiterock = "LettyWhiterock"
        /// 橙
        case chen = "Chen"
        /// アリス・マーガトロイド
        case aliceMargatroid = "AliceMargatroid"
        /// プリズムリバー三姉妹
        case prismriverSisters = "PrismriverSisters"
        /// 魂魄妖夢
        case youmuKonpaku = "YoumuKonpaku"
        /// 西行寺幽々子
        case yuyukoSaigyouji = "YuyukoSaigyouji"
        /// 八雲藍
        case ranYakumo = "RanYakumo"
        /// 八雲紫
        case yukariYakumo = "YukariYakumo"
    }
}
