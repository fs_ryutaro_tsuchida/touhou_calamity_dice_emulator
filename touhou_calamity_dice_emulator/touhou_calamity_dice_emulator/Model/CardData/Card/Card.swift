//
//  Card.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// カード
struct Card {
    /// カードID
    let cardId: CardId
    /// 勢力ID
    let tribeId: TribeId
    /// カード名
    let name: String
    /// 短縮名
    let shortName: String
    /// 能力一覧
    let abilities: [AbilityProtocol]
    /// コスト一覧
    let costs: [SingleReserve: Int]

    /// 入れ子の能力一覧
    var nestAbilities: [AbilityProtocol] {
        abilities.reduce(into: [AbilityProtocol]()) { result, ability in
            result.append(contentsOf: ability.nestAbilities)
        }
    }

    /// 全カード情報
    private static var fullCardInfo: [CardInfoProtocol] {
        [
            ReimuHakurei(),
            MarisaKirisame()
        ] + [
            Rumia(),
            Daiyousei(),
            Cirno(),
            HongMeiling()
        ]
    }

    /// 全カード一覧
    static var fullCard: [Card] {
        fullCardInfo.reduce(into: [Card]()) {
            $0.append(contentsOf: $1.cards)
        }
    }
}
