//
//  TribeId.swift
//  touhou_calamity_dice_emulator
//

extension Card {
    /// 勢力ID
    enum TribeId: Int, CaseIterable {
        /// 主人公勢力
        case playable = 0
        /// 紅魔郷勢力
        case embodimentOfScaletDevil = 1
        /// 妖々夢
        case perfectCherryBlossom = 2
        /// 永夜沙
        case imperishableNight = 3
        /// 風神録
        case mountainOfFaith = 4
        /// 地霊殿
        case subterraneanAnimism = 5
        /// 星蓮船
        case undefinedFantasticObject = 6
        /// 神霊廟
        case tenDesires = 7
        /// 輝針城
        case doubleDealingCharacter = 8
        /// 紺珠伝
        case legacyOfLunaticKingdom = 9
        /// 天空璋
        case hiddenStarInFourSeasons = 10
        /// スペルカード
        case spellCard = 51
    }
}
