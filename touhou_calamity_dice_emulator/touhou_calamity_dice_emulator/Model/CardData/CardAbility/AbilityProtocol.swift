//
//  AbilityProtocol.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 能力
protocol AbilityProtocol {
    /// 能力の固有名称
    var abilityName: String { get }
    /// 入れ子の能力一覧
    var nestAbilities: [AbilityProtocol] { get }
    /// 文章
    var text: String { get }
    /// 次のステップへ進めるのをキャンセルするか
    var isCancelNextStep: Bool { get }
}

extension AbilityProtocol {
    /// ID
    var id: String {
        abilityName
    }
}

/// 継続的に状況を変化させる能力
protocol BuffAbilityProtocol: AbilityProtocol {}

/// 実行可能な能力
protocol RunnableAbilityProtocol: AbilityProtocol {
    /// 実行処理
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - fromPlayerId: 実行元のプレイヤーID
    ///   - fromCardId: 実行元のカードID
    ///   - traceCardId: トレース用のカードID
    ///   - onSuccess: 成功時処理
    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    )
}

/// 召喚コストを変動させる能力
protocol ManipulateSummonCostAbilityProtocol: BuffAbilityProtocol {
    /// 対象プレイヤー種別
    var targetPlayer: AbilityAutoTargetPlayerType { get }
    /// 対象カード種別
    var targetCard: AbilityTargetCardType { get }

    /// 召喚コストの差分
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - card: 対象のカード
    func cost(
        _ game: Binding<GameViewModel>,
        card: GameModel.GameCard
    ) -> [MultiReserve: Int]

    /// 召喚コストの種別
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - card: 対象のカード
    func type(
        _ game: Binding<GameViewModel>,
        card: GameModel.GameCard
    ) -> ManipulateCostAbilityType
}

extension ManipulateSummonCostAbilityProtocol {
    /// 能力の対象か
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - playerId: 対象のプレイヤーID
    ///   - card: 対象のカード
    ///   - fromPlayerId: 実行元のプレイヤーID
    func isTarget(
        _ game: Binding<GameViewModel>,
        playerId: UUID,
        card: GameModel.GameCard,
        fromPlayerId: UUID
    ) -> Bool {
        let playerIds = targetPlayer.selectPlayer(game, fromPlayerId: fromPlayerId)
        let cardIds = targetCard.selectCard(game)
        return playerIds.contains(playerId) && cardIds.contains(card.id)
    }
}
