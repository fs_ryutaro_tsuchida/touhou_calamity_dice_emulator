//
//  DiceRollAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ダイスを振って出目が一致した時に発動する能力
struct DiceRollAbility: RunnableAbilityProtocol {
    var abilityName: String {
        let diceRollStr = successDiceRolls.map { "\($0)" }.joined(separator: "|")
        return "DiceRoll_\(diceRollStr)_\(diceRollAbility.abilityName)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + diceRollAbility.nestAbilities
    }

    let text: String

    let isCancelNextStep = true

    /// 成功の出目
    let successDiceRolls: [Int]
    /// 全ての出目
    let previewDiceRolls: [Int]
    /// 出目が一致した時に発動する能力
    let diceRollAbility: RunnableAbilityProtocol

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        game.sheet.wrappedValue.ability.diceRoll.playerId = fromPlayerId
        game.sheet.wrappedValue.ability.diceRoll.successDiceRolls = successDiceRolls
        game.sheet.wrappedValue.ability.diceRoll.previewDiceRolls = previewDiceRolls
        game.sheet.wrappedValue.ability.diceRoll.onResult = { isSuccess in
            if isSuccess {
                diceRollAbility.run(
                    game,
                    fromPlayerId: fromPlayerId,
                    fromCardId: fromCardId,
                    traceCardId: traceCardId,
                    onSuccess: onSuccess
                )
            } else {
                onSuccess()
            }
        }
        queueAnimation {
            game.sheet.wrappedValue.viewType = .ability_diceRoll
        }
    }
}
