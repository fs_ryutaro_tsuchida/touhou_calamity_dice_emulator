//
//  PlayerAdditionalAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤーに能力を追加する能力
struct PlayerAdditionalAbility: RunnableAbilityProtocol {
    var abilityName: String {
        "PlayerAdditional_\(additionalAbility.abilityName)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + additionalAbility.nestAbilities
    }

    let text: String

    let isCancelNextStep = true

    /// 追加する能力
    let additionalAbility: AbilityProtocol
    /// 終了タイミング
    let finishTiming: AbilityTiming?

    /// 対象プレイヤー種別
    let targetPlayer: AbilitySelectableTargetPlayerType

    /// 発動元カードがフィールドを離れた時にキャンセルするか
    let isCancelIfFromCardRemove: Bool

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        targetPlayer.selectPlayer(
            game,
            fromPlayerId: fromPlayerId
        ) { playerIds in
            game.game.players.filter {
                playerIds.contains($0.wrappedValue.id)
            }.forEach { player in
                player.wrappedValue.additionalAbilities.append(
                    AdditionalAbility(
                        abilityName: additionalAbility.abilityName,
                        finishTiming: finishTiming,
                        fromPlayerId: fromPlayerId,
                        fromCardId: fromCardId,
                        traceCardId: traceCardId
                    )
                )
            }
            onSuccess()
        }
    }
}
