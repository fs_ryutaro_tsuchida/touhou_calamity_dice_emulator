//
//  AutoTriggerAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 指定のタイミングで自動実行される能力
struct AutoTriggerAbility: BuffAbilityProtocol, RunnableAbilityProtocol {
    var abilityName: String {
        "AuthTrigger_\(runAbility.abilityName)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + runAbility.nestAbilities
    }

    let text: String

    let isCancelNextStep = false

    /// 発動タイミング
    let timing: AbilityTiming
    /// 実行される能力
    let runAbility: RunnableAbilityProtocol

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        runAbility.run(
            game,
            fromPlayerId: fromPlayerId,
            fromCardId: fromCardId,
            traceCardId: traceCardId,
            onSuccess: onSuccess
        )
    }
}
