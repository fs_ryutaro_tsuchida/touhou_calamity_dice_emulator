//
//  SelfCardAdditionalAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 自身に能力を追加する能力
struct SelfCardAdditionalAbility: RunnableAbilityProtocol {
    var abilityName: String {
        "PlayerAdditional_\(additionalAbility.abilityName)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + additionalAbility.nestAbilities
    }

    var text: String {
        additionalAbility.text
    }

    let isCancelNextStep = true

    /// 追加する能力
    let additionalAbility: AbilityProtocol
    /// 終了タイミング
    let finishTiming: AbilityTiming?

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        guard let card = game.game.cards.first(where: { card in
            card.wrappedValue.id == fromCardId
        }) else {
            return
        }
        if !card.wrappedValue.isShowAbilities,
           card.wrappedValue.additionalAbilities.isEmpty {
            card.wrappedValue.isShowAbilities = true
            card.wrappedValue.isUserOperatorOnShowAbilities = false
        }
        card.wrappedValue.additionalAbilities.append(
            AdditionalAbility(
                abilityName: additionalAbility.abilityName,
                finishTiming: finishTiming,
                fromPlayerId: fromPlayerId,
                fromCardId: fromCardId,
                traceCardId: traceCardId
            )
        )
        onSuccess()
    }
}
