//
//  MultiRunnableAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 複数同時に実行する能力
struct MultiRunnableAbility: RunnableAbilityProtocol {
    var abilityName: String {
        abilities.map(\.abilityName).joined(separator: "_And_")
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + abilities.reduce(into: [AbilityProtocol]()) {
            $0 += $1.nestAbilities
        }
    }

    let text: String

    let isCancelNextStep = true

    /// 実行する能力一覧
    let abilities: [RunnableAbilityProtocol]

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        var names = abilities.map(\.abilityName)
        abilities.forEach { ability in
            ability.run(
                game,
                fromPlayerId: fromPlayerId,
                fromCardId: fromCardId,
                traceCardId: traceCardId
            ) {
                if let index = names.firstIndex(of: ability.abilityName) {
                    names.remove(at: index)
                }
                if names.isEmpty {
                    onSuccess()
                }
            }
        }
    }
}
