//
//  CouplingAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// CP (カップリング) 能力
struct CouplingAbility<ChildAbility: AbilityProtocol>: AbilityProtocol {
    var abilityName: String {
        "\(couplingCardId.rawValue)_Coupling_\(couplingAbility.abilityName)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self] + couplingAbility.nestAbilities
    }

    let text: String

    var isCancelNextStep: Bool {
        couplingAbility.isCancelNextStep
    }

    /// カップリング先のカードのID
    let couplingCardId: Card.CardId
    /// カップリングが成立している時に有効になる能力
    let couplingAbility: ChildAbility

    /// カップリングが有効か
    /// - Parameters:
    ///   - game: ゲームデータ
    ///   - playerId: プレイヤーID
    /// - Returns: 有効:true
    func isEstablishedCP(_ game: GameModel, playerId: UUID) -> Bool {
        game.players[id: playerId].fieldCards(game).contains {
            $0.cardId == couplingCardId
        }
    }
}

extension CouplingAbility: BuffAbilityProtocol
    where ChildAbility: BuffAbilityProtocol {}

extension CouplingAbility: RunnableAbilityProtocol
    where ChildAbility: RunnableAbilityProtocol {
    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        guard isEstablishedCP(game.wrappedValue.game, playerId: fromPlayerId) else {
            return
        }
        couplingAbility.run(
            game,
            fromPlayerId: fromPlayerId,
            fromCardId: fromCardId,
            traceCardId: traceCardId,
            onSuccess: onSuccess
        )
    }
}
