//
//  CannotSummonAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚不可能力
struct CannotSummonAbility: BuffAbilityProtocol {
    let abilityName = "CannotSummon"

    var nestAbilities: [AbilityProtocol] {
        [self]
    }

    let text: String

    let isCancelNextStep = false
}
