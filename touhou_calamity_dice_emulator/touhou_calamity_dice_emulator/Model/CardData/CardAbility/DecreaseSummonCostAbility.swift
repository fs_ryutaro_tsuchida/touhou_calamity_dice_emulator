//
//  DecreaseSummonCostAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 召喚コストを減少させる能力
struct DecreaseSummonCostAbility: ManipulateSummonCostAbilityProtocol {
    var abilityName: String {
        let costStr = costs.sorted {
            $0.key < $1.key
        }.map {
            "\($0.key):\($0.value)"
        }.joined(separator: "|")
        return "DecreaseSummonCost_\(costStr)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self]
    }

    let text: String

    /// 召喚コストの差分
    let costs: [MultiReserve: Int]

    let targetPlayer: AbilityAutoTargetPlayerType

    let targetCard: AbilityTargetCardType

    let isCancelNextStep = false

    func cost(
        _ game: Binding<GameViewModel>,
        card: GameModel.GameCard
    ) -> [MultiReserve: Int] {
        costs
    }

    func type(
        _ game: Binding<GameViewModel>,
        card: GameModel.GameCard
    ) -> ManipulateCostAbilityType {
        .decrease
    }
}
