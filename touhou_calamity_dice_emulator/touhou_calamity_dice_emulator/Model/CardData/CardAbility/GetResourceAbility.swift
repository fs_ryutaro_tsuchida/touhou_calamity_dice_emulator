//
//  GetResourceAbility.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 資材獲得能力
struct GetResourceAbility: RunnableAbilityProtocol {
    var abilityName: String {
        let reserveStr = reserveChoices.sorted {
            $0.key < $1.key
        }.map {
            "\($0.key):\($0.value)"
        }.joined(separator: "|")
        return "GetResource_\(reserveStr)"
    }

    var nestAbilities: [AbilityProtocol] {
        [self]
    }

    let text: String

    let isCancelNextStep = true

    /// 獲得可能な資材
    let reserveChoices: [SingleReserve: Int]

    func run(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        fromCardId: UUID?,
        traceCardId: UUID,
        onSuccess: @escaping () -> Void
    ) {
        if let choice = reserveChoices.randomElement() {
            game.wrappedValue.game.players[id: fromPlayerId]
                .reserves[choice.key, default: 0] += choice.value
        }
        onSuccess()
    }
}
