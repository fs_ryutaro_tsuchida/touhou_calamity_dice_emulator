//
//  Player.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤー
struct Player: Identifiable {
    /// ID
    let id = UUID()
    /// プレイヤー名
    var name: String

    private let turnStartStep = PlayerTurnStep(stepType: .turnStart)
    private let playerDiceRollStep = PlayerTurnStep(stepType: .playerDiceRoll)
    private let actionStep = PlayerTurnStep(stepType: .action)
    private let turnFinishStep = PlayerTurnStep(stepType: .turnFinish)

    /// プレイヤーターンのステップ一覧
    var playerTurnSteps: [PlayerTurnStep] {
        [
            turnStartStep,
            playerDiceRollStep,
            actionStep,
            turnFinishStep
        ]
    }

    /// リザーブの獲得量
    var reserves: [SingleReserve: Int] = {
        SingleReserve.userReserves
            .reduce(into: [SingleReserve: Int]()) { result, reserve in
                result[reserve] = reserve.defaultValue
            }
    }() {
        didSet {
            checkReservesOverflow()
        }
    }

    /// リザーブの最大獲得量
    var maxReserves: [SingleReserve: Int] {
        SingleReserve.userReserves
            .reduce(into: [SingleReserve: Int]()) { result, reserve in
                result[reserve] = reserve.defaultMaxValue
            }
    }

    /// リザーブが最大獲得量を超えていないか確認、修正する
    private mutating func checkReservesOverflow() {
        // 切れ端が最大値を超えた場合、ループした回数分だけスペルゲージを増やす
        if reserves[.slip, default: 0] >= maxReserves[.slip, default: 0] {
            let value = reserves[.slip, default: 0] / maxReserves[.slip, default: 1]
            reserves[.slip, default: 0] -= (value * maxReserves[.slip, default: 1])
            reserves[.spellGage, default: 0] += value
        }

        // 資材・ゲージ・スペルゲージが最大値を超えた場合、最大値まで減らす
        SingleReserve.materials.forEach {
            if reserves[$0, default: 0] > maxReserves[$0, default: 0] {
                reserves[$0] = maxReserves[$0, default: 0]
            }
        }
        if reserves[.gage, default: 0] > maxReserves[.gage, default: 0] {
            reserves[.gage] = maxReserves[.gage, default: 0]
        }
        if reserves[.spellGage, default: 0] > maxReserves[.spellGage, default: 0] {
            reserves[.spellGage] = maxReserves[.gage, default: 0]
        }

        // 手札は別で処理する (ユーザー選択が入るため)
    }

    /// 場に出ているカードのID一覧
    var fieldCardIds: [UUID] = []

    /// 場に出ているカード一覧
    /// - Parameter game: ゲームデータ
    func fieldCards(_ game: GameModel) -> [GameModel.GameCard] {
        game.cards.filter {
            fieldCardIds.contains($0.id)
        }
    }

    /// 手札のカードのID一覧
    var handCardIds: [UUID] = []

    /// 追加された能力一覧
    var additionalAbilities: [AdditionalAbility] = []

    /// 次のステップに進める
    /// - Parameter game: ゲーム表示画面の表示データ
    func nextStep(_ game: Binding<GameViewModel>) {
        playerTurnSteps[id: game.wrappedValue.game.nowStep]
            .stepType.nextStep(game, playerId: id)
    }

    /// 可能なら次のステップに進める
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - interval: 実行タイミング
    func nextStepIfCan(
        _ game: Binding<GameViewModel>,
        deadline: DispatchTime = .now()
    ) {
        playerTurnSteps[id: game.wrappedValue.game.nowStep]
            .stepType.nextStepIfCan(game, playerId: id, deadline: deadline)
    }
}
