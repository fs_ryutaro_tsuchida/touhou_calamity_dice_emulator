//
//  AdditionalAbility.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// 追加された能力
struct AdditionalAbility: Identifiable {
    let id = UUID()
    /// 能力名
    let abilityName: String
    /// 終了タイミング
    let finishTiming: AbilityTiming?
    /// 追加元のプレイヤーのID
    let fromPlayerId: UUID
    /// 追加元のカードのID
    let fromCardId: UUID?
    /// トレース用のカードのID
    let traceCardId: UUID

    /// 能力
    /// - Parameter game: ゲーム表示画面の表示データ
    func ability(_ game: GameViewModel) -> AbilityProtocol! {
        game.game.cards[id: traceCardId].card.nestAbilities.first {
            $0.abilityName == abilityName
        }
    }
}
