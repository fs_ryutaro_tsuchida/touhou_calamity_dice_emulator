//
//  GameCard.swift
//  touhou_calamity_dice_emulator
//

import Foundation

extension GameModel {
    /// ゲーム上のカード情報
    struct GameCard: Identifiable {
        /// ID
        let id = UUID()
        /// カードID
        let cardId: Card.CardId
        /// カード
        var card: Card! {
            Card.fullCard.first { $0.cardId == cardId }
        }

        /// 場に出しているプレイヤー
        /// - Parameter game: ゲームデータ
        func fieldPlayer(_ game: GameModel) -> Player? {
            game.players.first {
                $0.fieldCardIds.contains(id)
            }
        }

        /// 場に出しているプレイヤーのID
        /// - Parameter game: ゲームデータ
        func fieldPlayerId(_ game: GameModel) -> UUID? {
            fieldPlayer(game)?.id
        }

        /// 追加された能力一覧
        var additionalAbilities: [AdditionalAbility] = []
        /// 能力を表示するか
        var isShowAbilities = false
        /// ユーザーが能力表示を操作したか
        var isUserOperatorOnShowAbilities = false
    }
}
