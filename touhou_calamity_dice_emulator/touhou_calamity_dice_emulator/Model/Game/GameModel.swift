//
//  GameModel.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ゲームデータ
struct GameModel {
    /// プレイヤー一覧
    var players: [Player] {
        didSet {
            if !turns.contains(id: nowTurn),
               let startRoundId = turns.first(where: {
                   $0.type == .roundStart
               })?.id {
                nowStep = startRoundId
            }
        }
    }

    /// プレイヤーターンのシフト数
    var turnShift: Int = 0 {
        didSet {
            if turnShift != (turnShift % players.count) {
                turnShift = turnShift % players.count
            }
        }
    }

    /// シフト数を反映したプレイヤー一覧
    var shiftPlayer: [Player] {
        var players = players
        (0..<turnShift).forEach { _ in
            players.append(players[0])
            players.remove(at: 0)
        }
        return players
    }

    /// ラウンド開始のターン定義
    private let roundStartTurn = Turn(id: .init(), type: .roundStart)
    /// ラウンド終了のターン定義
    private let roundFinishTurn = Turn(id: .init(), type: .roundFinish)
    /// ターン一覧
    var turns: [Turn] {
        var turns = [Turn]()
        turns.append(roundStartTurn)

        turns.append(contentsOf: shiftPlayer.compactMap {
            Turn(id: $0.id, type: .player)
        })

        turns.append(roundFinishTurn)
        return turns
    }

    private let roundStartStep = RoundStartStep(stepType: .roundStart)
    private let calamityStartStep = RoundStartStep(stepType: .calamityStart)

    /// ラウンド開始のステップ一覧
    var roundStartSteps: [RoundStartStep] {
        [
            roundStartStep,
            calamityStartStep
        ]
    }

    private let calamityFinishStep = RoundFinishStep(stepType: .calamityFinish)
    private let roundFinishStep = RoundFinishStep(stepType: .roundFinish)

    /// ラウンド終了のステップ一覧
    var roundFinishSteps: [RoundFinishStep] {
        [
            calamityFinishStep,
            roundFinishStep
        ]
    }

    /// 現在のステップのID
    var nowStep = UUID()
    /// 現在のターンのID
    var nowTurn: UUID {
        get {
            if roundStartSteps.contains(id: nowStep) {
                return roundStartTurn.id
            } else if let playerId = players.first(where: {
                $0.playerTurnSteps.contains(id: nowStep)
            })?.id {
                return playerId
            } else if roundFinishSteps.contains(id: nowStep) {
                return roundFinishTurn.id
            } else {
                return .init()
            }
        }
        // swiftlint:disable:next unused_setter_value
        set {
            // 何もしない
        }
    }

    /// カード一覧
    var cards: [GameCard] = Card.fullCard.map {
        GameCard(cardId: $0.cardId)
    }

    /// 所在不明なカードのID一覧
    var unknownCardIds: [UUID] {
        cards.map(\.id).filter { cardId in
            !players.contains { player in
                player.fieldCardIds.contains(cardId)
                    || player.handCardIds.contains(cardId)
            }
        }
    }
}
