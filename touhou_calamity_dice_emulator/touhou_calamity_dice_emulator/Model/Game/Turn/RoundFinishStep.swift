//
//  RoundFinishStep.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ラウンド終了のステップ定義
struct RoundFinishStep: Hashable, Identifiable {
    /// ステップ種別
    enum StepType: Hashable {
        /// 異変終了
        case calamityFinish
        /// ラウンド終了
        case roundFinish

        /// 表示名
        var name: String {
            switch self {
            case .calamityFinish:
                return "異変終了"
            case .roundFinish:
                return "R 終了"
            }
        }

        /// 次のステップ
        var next: StepType? {
            switch self {
            case .calamityFinish:
                return .roundFinish
            case .roundFinish:
                return nil
            }
        }

        /// 自動で次のステップに進めるか
        var canAutoNextStep: Bool {
            true
        }

        /// 次のステップに進める
        /// - Parameter game: ゲーム表示画面の表示データ
        func nextStep(_ game: Binding<GameViewModel>) {
            if let next = next {
                if let nextStep = game.wrappedValue.game.roundFinishSteps.first(where: {
                    $0.stepType == next
                }) {
                    game.wrappedValue.game.nowStep = nextStep.id
                    switch next {
                    case .roundFinish:
                        game.finishAndRunAbilities { timing, _, _ in
                            timing == .roundFinish
                        }
                    case .calamityFinish:
                        break
                    }
                }
                next.nextStepIfCan(
                    game,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            } else {
                game.nextTurn()
            }
        }

        /// 可能なら次のステップに進める
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - deadline: 実行タイミング
        func nextStepIfCan(
            _ game: Binding<GameViewModel>,
            deadline: DispatchTime = .now()
        ) {
            guard canAutoNextStep,
                  !game.wrappedValue.game.cards.contains(where: { card in
                      card.additionalAbilities.contains(where: {
                          $0.ability(game.wrappedValue).isCancelNextStep
                      })
                  }) else {
                return
            }
            DispatchQueue.main.asyncAfter(
                deadline: deadline,
                execute: DispatchWorkItem {
                    withAnimation {
                        self.nextStep(game)
                    }
                }
            )
        }
    }

    let id = UUID()
    /// ステップ種別
    let stepType: StepType
}
