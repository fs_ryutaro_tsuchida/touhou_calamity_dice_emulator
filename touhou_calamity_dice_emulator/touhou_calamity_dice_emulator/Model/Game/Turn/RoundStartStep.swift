//
//  RoundStartStep.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ラウンド開始のステップ定義
struct RoundStartStep: Hashable, Identifiable {
    /// ステップ種別
    enum StepType: Hashable {
        /// ラウンド開始
        case roundStart
        /// 異変開始
        case calamityStart

        /// 表示名
        var name: String {
            switch self {
            case .roundStart:
                return "R 開始"
            case .calamityStart:
                return "異変開始"
            }
        }

        /// 次のステップ
        var next: StepType? {
            switch self {
            case .roundStart:
                return .calamityStart
            case .calamityStart:
                return nil
            }
        }

        /// 自動で次のステップに進めるか
        var canAutoNextStep: Bool {
            // TODO: 異変ダイス
            true
//            switch self {
//            case .roundStart:
//                return true
//            case .calamityStart:
//                return false
//            }
        }

        /// 次のステップに進める
        /// - Parameter game: ゲーム表示画面の表示データ
        func nextStep(_ game: Binding<GameViewModel>) {
            if let next = next {
                if let nextStep = game.wrappedValue.game.roundStartSteps.first(where: {
                    $0.stepType == next
                }) {
                    game.wrappedValue.game.nowStep = nextStep.id
                }
                next.nextStepIfCan(
                    game,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            } else {
                game.nextTurn()
            }
        }

        /// 可能なら次のステップに進める
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - deadline: 実行タイミング
        func nextStepIfCan(
            _ game: Binding<GameViewModel>,
            deadline: DispatchTime = .now()
        ) {
            guard canAutoNextStep,
                  !game.wrappedValue.game.cards.contains(where: { card in
                      card.additionalAbilities.contains(where: {
                          $0.ability(game.wrappedValue).isCancelNextStep
                      })
                  }) else {
                return
            }
            DispatchQueue.main.asyncAfter(
                deadline: deadline,
                execute: DispatchWorkItem {
                    withAnimation {
                        self.nextStep(game)
                    }
                }
            )
        }
    }

    let id = UUID()
    /// ステップ種別
    let stepType: StepType
}
