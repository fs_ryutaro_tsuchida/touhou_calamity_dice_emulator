//
//  Turn.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// ターン定義
struct Turn: Hashable, Identifiable {
    /// ターン種別
    enum TurnType: Hashable {
        /// ラウンド開始
        case roundStart
        /// プレイヤーターン
        case player
        /// ラウンド終了
        case roundFinish

        /// 表示名
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - turnId: ターン定義のID
        /// - Returns: 表示名
        func name(_ game: GameViewModel, turnId: UUID) -> String {
            switch self {
            case .roundStart:
                return "R 開始"
            case .player:
                return game.game.players[id: turnId].name
            case .roundFinish:
                return "R 終了"
            }
        }

        /// 自動で次のターンに進めるか
        var canAutoNextTurn: Bool {
            switch self {
            case .roundStart, .roundFinish:
                return true
            case .player:
                return false
            }
        }
    }

    let id: UUID

    /// ターン種別
    let type: TurnType

    /// 表示名
    /// - Parameter game: ゲーム表示画面の表示データ
    /// - Returns: 表示名
    func name(_ game: GameViewModel) -> String {
        type.name(game, turnId: id)
    }

    /// 次のターン
    /// - Parameter game: ゲーム表示画面の表示データ
    /// - Returns: 次のターン
    static func next(_ game: GameModel) -> Turn? {
        if game.turns.last?.id == game.nowTurn {
            return game.turns.first {
                $0.type == .roundStart
            }
        } else if let nowTurnIndex = game.turns
            .firstIndex(id: game.nowTurn) {
            return game.turns[nowTurnIndex + 1]
        }
        return nil
    }

    /// 次のターンに進める
    /// - Parameter game: ゲーム表示画面の表示データ
    static func nextTurn(_ game: Binding<GameViewModel>) {
        guard let turn = next(game.wrappedValue.game) else {
            return
        }
        switch turn.type {
        case .roundStart:
            if let roundStart = game.wrappedValue.game.roundStartSteps.first(where: {
                $0.stepType == .roundStart
            }) {
                game.wrappedValue.game.nowStep = roundStart.id
                game.finishAndRunAbilities { timing, _, _ in
                    timing == .roundStart
                }
                roundStart.stepType.nextStepIfCan(
                    game,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            }
        case .player:
            let player = game.wrappedValue.game.players[id: turn.id]
            if let turnStart = player.playerTurnSteps.first(where: {
                $0.stepType == .turnStart
            }) {
                game.wrappedValue.game.nowStep = turnStart.id
                game.wrappedValue.nowSelectPlayerId = player.id
                turnStart.stepType.nextStepIfCan(
                    game,
                    playerId: player.id,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            }
        case .roundFinish:
            if let calamityFinish = game.wrappedValue.game.roundFinishSteps.first(where: {
                $0.stepType == .calamityFinish
            }) {
                game.wrappedValue.game.nowStep = calamityFinish.id
                game.wrappedValue.game.turnShift += 1
                calamityFinish.stepType.nextStepIfCan(
                    game,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            }
        }
    }
}
