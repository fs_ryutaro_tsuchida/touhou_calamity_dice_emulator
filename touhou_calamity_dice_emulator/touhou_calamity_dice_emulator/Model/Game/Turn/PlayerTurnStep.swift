//
//  PlayerTurnStep.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// プレイヤーターンのステップ定義
struct PlayerTurnStep: Hashable, Identifiable {
    /// ステップ種別
    enum StepType: Hashable {
        /// ターン終了
        case turnStart
        /// プレイヤーダイスを振る
        case playerDiceRoll
        /// アクション
        case action
        /// ターン終了
        case turnFinish

        /// 表示名
        var name: String {
            switch self {
            case .turnStart:
                return "T 開始"
            case .playerDiceRoll:
                return "PLダイス"
            case .action:
                return "アクション"
            case .turnFinish:
                return "T 終了"
            }
        }

        /// 次のステップ
        var next: StepType? {
            switch self {
            case .turnStart:
                return .playerDiceRoll
            case .playerDiceRoll:
                return .action
            case .action:
                return .turnFinish
            case .turnFinish:
                return nil
            }
        }

        /// 自動で次のステップに進めるか
        var canAutoNextStep: Bool {
            switch self {
            case .turnStart, .turnFinish:
                return true
            case .playerDiceRoll, .action:
                return false
            }
        }

        /// 次のステップに進める
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - playerId: プレイヤーID
        func nextStep(_ game: Binding<GameViewModel>, playerId: UUID) {
            let player = game.wrappedValue.game.players[id: playerId]
            if let next = next {
                if let nextStep = player.playerTurnSteps.first(where: {
                    $0.stepType == next
                }) {
                    game.wrappedValue.game.nowStep = nextStep.id
                }
                next.nextStepIfCan(
                    game,
                    playerId: playerId,
                    deadline: .now().advanced(by: .milliseconds(500))
                )
            } else {
                game.nextTurn()
            }
        }

        /// 可能なら次のステップに進める
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - playerId: プレイヤーID
        ///   - deadline: 実行タイミング
        func nextStepIfCan(
            _ game: Binding<GameViewModel>,
            playerId: UUID,
            deadline: DispatchTime = .now()
        ) {
            guard canAutoNextStep,
                  !game.wrappedValue.game.cards.contains(where: { card in
                      card.additionalAbilities.contains(where: {
                          $0.ability(game.wrappedValue).isCancelNextStep
                      })
                  }) else {
                return
            }
            DispatchQueue.main.asyncAfter(
                deadline: deadline,
                execute: DispatchWorkItem {
                    withAnimation {
                        self.nextStep(game, playerId: playerId)
                    }
                }
            )
        }
    }

    let id = UUID()
    /// ステップ種別
    let stepType: StepType
}
