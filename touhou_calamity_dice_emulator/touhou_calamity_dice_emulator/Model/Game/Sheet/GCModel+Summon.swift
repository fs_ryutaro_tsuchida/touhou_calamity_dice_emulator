//
//  GCModel+Summon.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension GameSheetModel {
    /// 召喚
    struct Summon {
        /// プレイヤーID
        var id = UUID()
    }
}
