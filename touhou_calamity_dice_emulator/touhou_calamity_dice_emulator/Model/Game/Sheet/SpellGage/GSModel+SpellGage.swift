//
//  GSModel+SpellGage.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension GameSheetModel {
    /// スペルゲージ
    struct SpellGage {
        /// 資材獲得
        var getReserve = GetReserve()
    }
}
