//
//  GSMSpellGage+GetReserve.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension GameSheetModel.SpellGage {
    /// 資材獲得
    struct GetReserve {
        /// プレイヤーID
        var id = UUID()
    }
}
