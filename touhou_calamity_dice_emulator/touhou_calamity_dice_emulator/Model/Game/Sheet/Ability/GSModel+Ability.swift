//
//  GSModel+Ability.swift
//  touhou_calamity_dice_emulator
//

extension GameSheetModel {
    /// カード効果
    struct Ability {
        /// ダイスロール
        var diceRoll = DiceRoll()
    }
}
