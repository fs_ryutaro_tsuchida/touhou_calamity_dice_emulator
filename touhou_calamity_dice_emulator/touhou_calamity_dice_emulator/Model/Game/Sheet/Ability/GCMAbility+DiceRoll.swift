//
//  GCMAbility+DiceRoll.swift
//  touhou_calamity_dice_emulator
//

import UIKit

extension GameSheetModel.Ability {
    /// ダイスロール
    struct DiceRoll {
        /// プレイヤーID
        var playerId: UUID = .init()
        /// 成功の出目
        var successDiceRolls = [Int](0...4)
        /// 全ての出目
        var previewDiceRolls = [Int](0...9)
        /// ダイスロール結果
        var onResult: (Bool) -> Void = { _ in }
    }
}
