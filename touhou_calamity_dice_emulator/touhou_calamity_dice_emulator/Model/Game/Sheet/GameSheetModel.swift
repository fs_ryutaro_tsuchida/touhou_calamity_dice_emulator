//
//  GameSheetModel.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// シート表示データ
struct GameSheetModel {
    /// 表示する画面種別
    enum ViewType {
        /// プレイヤー名変更
        case playerName
        /// SG - 資材獲得
        case spellGage_getReserve
        /// 召喚
        case summon
        /// ダイスロール
        case ability_diceRoll

        /// 画面の表示サイズの一覧
        var detents: [UISheetPresentationController.Detent] {
            switch self {
            case .playerName, .spellGage_getReserve, .ability_diceRoll:
                return [.medium()]
            case .summon:
                return [.large()]
            }
        }

        /// 画面の表示内容
        /// - Parameters:
        ///   - game: ゲーム表示画面の表示データ
        ///   - sheet: シート表示データ
        /// - Returns: 表示内容
        @ViewBuilder
        func body(
            game: Binding<GameViewModel>,
            sheet: Binding<GameSheetModel>
        ) -> some View {
            if self == .playerName {
                NavigationView {
                    PlayerNameView(
                        game: game,
                        name: game.wrappedValue.game.players[
                            id: sheet.wrappedValue.playerName.id
                        ].name
                    )
                }
            } else if self == .spellGage_getReserve {
                NavigationView {
                    SpellGageGetReserveView(game: game)
                }
            } else if self == .summon {
                NavigationView {
                    SummonView(game: game)
                }
            } else if self == .ability_diceRoll {
                NavigationView {
                    AbilityDiceSelectView(game: game)
                }
            }
        }
    }

    /// 表示する画面種別
    /// - remark: nilの場合は非表示
    var viewType: ViewType?

    /// シート表示中か
    var isPresented: Bool {
        get {
            viewType != nil
        }
        set {
            if !newValue {
                viewType = nil
            }
        }
    }

    /// プレイヤー名入力
    var playerName = PlayerName()

    /// スペルゲージ
    var spellGage = SpellGage()

    /// 召喚
    var summon = Summon()

    /// カード効果
    var ability = Ability()

    /// 表示拡張
    struct Modifier: ViewModifier {
        /// ゲーム表示画面の表示データ
        @Binding var game: GameViewModel
        /// シート表示データ
        @Binding var sheet: GameSheetModel

        @ViewBuilder
        func body(content: Content) -> some View {
            content.sheet(
                isPresented: $sheet.isPresented,
                detents: sheet.viewType?.detents ?? [.medium(), .large()]
            ) {
                sheet.viewType?.body(game: $game, sheet: $sheet)
            }
        }
    }
}

extension Binding where Value == GameSheetModel {
    /// 表示拡張
    /// - Parameter game: ゲーム表示画面の表示データ
    /// - Returns: 表示拡張
    func modifier(_ game: Binding<GameViewModel>) -> Value.Modifier {
        .init(game: game, sheet: self)
    }
}
