//
//  GSModel+PlayerName.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

extension GameSheetModel {
    /// プレイヤー名入力
    struct PlayerName {
        /// プレイヤーID
        var id = UUID()
    }
}
