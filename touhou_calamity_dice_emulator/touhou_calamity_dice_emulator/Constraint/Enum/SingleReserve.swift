//
//  SingleReserve.swift
//  touhou_calamity_dice_emulator
//

import UIKit

/// 単体のリザーブ種別
enum SingleReserve: CaseIterable, Hashable, Identifiable, Comparable {
    /// 酒
    case liquor
    /// 金
    case money
    /// 本
    case book
    /// 力
    case power
    /// 切れ端
    case slip
    /// ゲージ
    case gage
    /// 手札
    case handCard
    /// スペルゲージ
    case spellGage
    /// 捨て札
    case trushCard

    var id: Int {
        switch self {
        case .liquor:
            return 1
        case .money:
            return 2
        case .book:
            return 3
        case .power:
            return 4
        case .slip:
            return 5
        case .gage:
            return 6
        case .handCard:
            return 7
        case .spellGage:
            return 8
        case .trushCard:
            return 11
        }
    }

    /// リザーブ名
    var name: String {
        switch self {
        case .liquor:
            return "酒"
        case .money:
            return "金"
        case .book:
            return "本"
        case .power:
            return "力"
        case .slip:
            return "切れ端"
        case .gage:
            return "ゲージ"
        case .handCard:
            return "手札"
        case .spellGage:
            return "スペルゲージ"
        case .trushCard:
            return "捨て札"
        }
    }

    /// 初期獲得量
    var defaultValue: Int {
        switch self {
        case .gage:
            return 3
        case .handCard:
            return 9
        case .spellGage:
            return 3
        case .liquor, .money, .book, .power, .slip:
            return 0
        default:
            assertionFailure("\(self).defaultValue is defined")
            return 0
        }
    }

    /// 初期時点の最大獲得量
    var defaultMaxValue: Int {
        switch self {
        case .slip:
            return 6
        case .gage:
            return 10
        case .handCard:
            return 10
        case .liquor, .money, .book, .power, .spellGage:
            return 3
        default:
            assertionFailure("\(self).defaultMaxValue is defined")
            return 0
        }
    }

    /// 資材か
    var isMaterial: Bool {
        switch self {
        case .liquor, .money, .book, .power:
            return true
        default:
            return false
        }
    }

    /// ユーザーのリザーブか
    var isUserReserve: Bool {
        switch self {
        case .liquor, .money, .book, .power, .slip, .gage, .handCard, .spellGage:
            return true
        default:
            return false
        }
    }

    /// ファイル名
    /// 画像や文字色の取得に使用される
    var fileName: String {
        let name = "\(self)"
        let first = name.first?.uppercased() ?? ""
        let other = String(name.dropFirst())
        return first + other
    }

    /// アイコン名
    var iconName: String {
        "IconReserve\(fileName)"
    }

    /// アイコン画像
    var icon: UIImage? {
        UIImage(named: iconName)
    }

    /// 色名
    var colorName: String {
        "ColorReserve\(fileName)"
    }

    /// 表示色
    var color: UIColor? {
        UIColor(named: colorName)
    }

    /// 資材一覧
    static var materials: [Self] {
        allCases.filter(\.isMaterial)
    }

    /// ユーザーのリザーブ一覧
    static var userReserves: [Self] {
        allCases.filter(\.isUserReserve)
    }
}
