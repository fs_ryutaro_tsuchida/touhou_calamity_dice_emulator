//
//  MultiReserve.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// 複合リザーブ種別
enum MultiReserve: CaseIterable, Hashable, Identifiable, Comparable {
    /// 単独
    /// - Parameters reserve: リザーブ種別
    case single(_ reserve: SingleReserve)
    /// いずれかの資材
    case anyMaterial
    /// いずれかのリザーブ
    case anyReserve

    static var allCases: [MultiReserve] {
        var allCases: [MultiReserve] = SingleReserve.userReserves.map { .single($0) }
        allCases.append(.anyMaterial)
        allCases.append(.anyReserve)
        return allCases
    }

    var id: Int {
        switch self {
        case let .single(reserve):
            return reserve.id
        case .anyMaterial:
            return 21
        case .anyReserve:
            return 22
        }
    }

    /// リザーブ名
    var name: String {
        switch self {
        case let .single(reserve):
            return reserve.name
        case .anyMaterial:
            return "資材"
        case .anyReserve:
            return "コスト"
        }
    }

    /// リザーブ一覧
    var reserves: [SingleReserve] {
        switch self {
        case let .single(reserve):
            return [reserve]
        case .anyMaterial:
            return SingleReserve.materials
        case .anyReserve:
            return SingleReserve.allCases
        }
    }
}
