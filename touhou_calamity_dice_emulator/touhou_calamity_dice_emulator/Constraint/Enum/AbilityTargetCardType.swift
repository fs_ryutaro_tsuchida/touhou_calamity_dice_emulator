//
//  AbilityTargetCardType.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 能力対象カード種別
enum AbilityTargetCardType {
    /// 全てのカード
    case all
    /// 特定カード
    /// - Parameter cardIds: カードID一覧
    case card(cardIds: [Card.CardId])
    /// 特定勢力
    /// - Parameter tribeId: 勢力ID
    case tribe(tribeId: Card.TribeId)

    /// 対象カード一覧
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - fromCardId: 実行元のカードID
    func selectCard(
        _ game: Binding<GameViewModel>
    ) -> [UUID] {
        switch self {
        case .all:
            return game.wrappedValue.game.cards.map(\.id)
        case let .card(cardIds):
            return game.wrappedValue.game.cards.filter {
                cardIds.contains($0.card.cardId)
            }.map(\.id)
        case let .tribe(tribeId):
            return game.wrappedValue.game.cards.filter {
                $0.card.tribeId == tribeId
            }.map(\.id)
        }
    }
}
