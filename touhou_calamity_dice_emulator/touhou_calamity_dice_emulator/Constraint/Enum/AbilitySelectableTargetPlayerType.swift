//
//  AbilitySelectableTargetPlayerType.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 選択可能な能力対象プレイヤー種別
enum AbilitySelectableTargetPlayerType {
    /// 自分
    case selfPlayer
    /// 対戦相手1人
    case singleInOpponent
    /// 全ての対戦相手
    case multipleInOpponent
    /// いずれかのプレイヤー1人
    case singleInAllPlayer
    /// 全てのプレイヤー
    case multipleInAllPlayer

    /// 対象プレイヤー選択
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - fromPlayerId: 実行元のプレイヤーID
    ///   - onSelect: 選択後処理
    ///   - playerIds: 選択したプレイヤーのID一覧
    func selectPlayer(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID,
        onSelect: @escaping (_ playerIds: [UUID]) -> Void
    ) {
        switch self {
        case .selfPlayer:
            onSelect([fromPlayerId])
        case .singleInOpponent:
            // TODO: プレイヤー選択
            onSelect([
                game.wrappedValue.game.players.filter {
                    $0.id != fromPlayerId
                }.randomElement()?.id ?? fromPlayerId
            ])
        case .multipleInOpponent:
            onSelect(game.wrappedValue.game.players.filter {
                $0.id != fromPlayerId
            }.map(\.id))
        case .singleInAllPlayer:
            // TODO: プレイヤー選択
            onSelect([
                game.wrappedValue.game.players.randomElement()?.id ?? fromPlayerId
            ])
        case .multipleInAllPlayer:
            onSelect(game.wrappedValue.game.players.map(\.id))
        }
    }
}
