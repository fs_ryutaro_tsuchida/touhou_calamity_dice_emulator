//
//  ManipulateCostAbilityType.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// コストを変動させる能力の種別
enum ManipulateCostAbilityType: CaseIterable, Comparable {
    /// 増加
    case increase
    /// 減少
    case decrease

    /// 正負フラグ
    var signum: Int {
        switch self {
        case .increase:
            return 1
        case .decrease:
            return -1
        }
    }

    /// フィルタが必要か
    var needFilter: Bool {
        switch self {
        case .increase:
            return false
        case .decrease:
            return true
        }
    }
}
