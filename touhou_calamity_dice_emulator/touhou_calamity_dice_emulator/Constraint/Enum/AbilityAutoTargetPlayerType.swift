//
//  AbilityAutoTargetPlayerType.swift
//  touhou_calamity_dice_emulator
//

import SwiftUI

/// 自動決定される能力対象プレイヤー種別
enum AbilityAutoTargetPlayerType {
    /// 自分
    case selfPlayer
    /// 全ての対戦相手
    case allOpponent
    /// 全てのプレイヤー
    case allPlayer

    /// 対象プレイヤー一覧
    /// - Parameters:
    ///   - game: ゲーム表示画面の表示データ
    ///   - fromPlayerId: 実行元のプレイヤーID
    func selectPlayer(
        _ game: Binding<GameViewModel>,
        fromPlayerId: UUID
    ) -> [UUID] {
        switch self {
        case .selfPlayer:
            return [fromPlayerId]
        case .allOpponent:
            return game.wrappedValue.game.players.filter {
                $0.id != fromPlayerId
            }.map(\.id)
        case .allPlayer:
            return game.wrappedValue.game.players.map(\.id)
        }
    }
}
