//
//  AbilityTiming.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// 能力発動タイミング
enum AbilityTiming: Equatable {
    /// 召喚時
    case summon

    /// プレイヤーダイスの前
    case beforeDice

    /// カードの効果を受けた時
    case receivingCardAbility
    /// 異変選択時
    /// - Parameter diceRolls: 異変ダイスの出目一覧
    case selectCalamity(diceRolls: [Int])
    /// 異変が発動した時
    /// - Parameter diceRolls: 異変ダイスの出目一覧
    case activeCalamity(diceRolls: [Int])

    /// 自分に異変が起きた時
    /// - Parameter diceRolls: 異変ダイスの出目一覧
    case receivingSelfCalamity(diceRolls: [Int])
    /// 対戦相手に異変が起きた時
    /// - Parameter diceRolls: 異変ダイスの出目一覧
    case receivingOpponentCalamity(diceRolls: [Int])
    /// 自分が資材を使用した時
    /// - Parameter reserves: 資材一覧
    case useSelfReserve(reserves: [SingleReserve])
    /// 対戦相手が資材を使用した時
    /// - Parameter reserves: 資材一覧
    case useOpponentReserve(reserves: [SingleReserve])
    /// 一定枚数捨てる度
    /// - Parameter count: 捨てる枚数
    case toDiscord(count: Int)

    /// 次のラウンド
    case roundStart
    /// ラウンド終了
    case roundFinish
    /// 任意のタイミング
    case arbitrary

    /// カウントが上がった時
    /// - Parameter count: 累計カウント数
    case countUp(count: Int)
}
