//
//  CalamityType.swift
//  touhou_calamity_dice_emulator
//

import Foundation

/// 異変種別
enum CalamityType: Int, CaseIterable {
    /// 紅霧異変
    case bloodyMist = 1
    /// 春雪異変
    case snowInSpring = 2
    /// 永夜異変
    case alwaysNight = 3
    /// 現神異変
    case godStillHuman = 4
    /// 地霊異変
    case spiritFromUnderground = 5
    /// 宝船異変
    case treasureShip = 6
    /// 神霊異変
    case greedSpirit = 7
    /// 逆様異変
    case upsideDownCastle = 8
    /// 侵星異変
    case erosionMoon = 9
    /// 四季異変
    case fourSeasons = 0

    /// 効果詳細ID
    var abilityDetailId: Int {
        switch self {
        case .bloodyMist:
            return 0
        case .snowInSpring:
            return 1
        case .alwaysNight:
            return 2
        case .godStillHuman:
            return 3
        case .spiritFromUnderground:
            return 4
        case .treasureShip:
            return 5
        case .greedSpirit:
            return 6
        case .upsideDownCastle:
            return 7
        case .erosionMoon:
            return 8
        case .fourSeasons:
            return 9
        }
    }

    /// 表示名
    var name: String {
        switch self {
        case .bloodyMist:
            return "紅霧異変"
        case .snowInSpring:
            return "春雪異変"
        case .alwaysNight:
            return "永夜異変"
        case .godStillHuman:
            return "現神異変"
        case .spiritFromUnderground:
            return "地霊異変"
        case .treasureShip:
            return "宝船異変"
        case .greedSpirit:
            return "神霊異変"
        case .upsideDownCastle:
            return "逆様異変"
        case .erosionMoon:
            return "侵星異変"
        case .fourSeasons:
            return "四季異変"
        }
    }
}
