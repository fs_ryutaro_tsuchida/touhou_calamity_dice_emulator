#!/bin/bash

cd `dirname $0`

# ドキュメント生成
rm -rf ../public
jazzy -o ../public
git add ../public/.
git reset HEAD ../public/undocumented.json

# フォルダーソート
curl https://raw.githubusercontent.com/WebKit/webkit/master/Tools/Scripts/sort-Xcode-project-file > sort-Xcode-project-file
perl sort-Xcode-project-file touhou_calamity_dice_emulator.xcodeproj